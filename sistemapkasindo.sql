-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 01 Feb 2023 pada 16.07
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemapkasindo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_akses`
--

CREATE TABLE `tb_akses` (
  `id_akses` int(5) NOT NULL,
  `level` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `type_menu` enum('main','sub') NOT NULL,
  `akses` int(5) NOT NULL,
  `c` int(5) NOT NULL,
  `r` int(5) NOT NULL,
  `u` int(5) NOT NULL,
  `d` int(5) NOT NULL,
  `urut` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_akses`
--

INSERT INTO `tb_akses` (`id_akses`, `level`, `menu_id`, `type_menu`, `akses`, `c`, `r`, `u`, `d`, `urut`) VALUES
(1, 1, 1, 'main', 1, 1, 1, 1, 1, 1),
(2, 1, 1, 'sub', 1, 1, 1, 1, 1, 0),
(3, 1, 2, 'sub', 1, 1, 1, 1, 1, 0),
(4, 1, 2, 'main', 1, 1, 1, 1, 1, 2),
(5, 1, 3, 'sub', 0, 1, 1, 1, 1, 0),
(6, 1, 4, 'sub', 0, 1, 1, 1, 1, 0),
(7, 1, 3, 'main', 1, 1, 1, 1, 1, 4),
(8, 1, 5, 'sub', 1, 1, 1, 1, 1, 0),
(9, 1, 6, 'sub', 1, 1, 1, 1, 1, 0),
(10, 1, 4, 'main', 1, 1, 1, 1, 1, 5),
(11, 1, 7, 'sub', 1, 1, 1, 1, 1, 0),
(12, 1, 8, 'sub', 1, 1, 1, 1, 1, 0),
(13, 1, 5, 'main', 0, 0, 1, 1, 1, 6),
(14, 1, 6, 'main', 0, 0, 1, 1, 1, 7),
(15, 1, 9, 'sub', 1, 1, 1, 1, 1, 0),
(16, 1, 10, 'sub', 0, 1, 1, 1, 1, 0),
(17, 4, 7, 'main', 1, 1, 1, 1, 1, 0),
(18, 4, 8, 'main', 1, 1, 1, 1, 1, 0),
(19, 5, 8, 'main', 1, 1, 1, 1, 1, 0),
(20, 3, 8, 'main', 1, 1, 1, 1, 1, 0),
(21, 1, 9, 'main', 1, 1, 1, 1, 1, 3),
(22, 1, 11, 'sub', 1, 1, 1, 1, 1, 0),
(23, 2, 1, 'sub', 1, 1, 1, 1, 1, 0),
(24, 2, 1, 'main', 1, 1, 1, 1, 1, 1),
(25, 2, 2, 'main', 1, 1, 1, 1, 1, 2),
(26, 6, 1, 'main', 1, 1, 1, 1, 1, 1),
(27, 6, 6, 'sub', 1, 1, 1, 1, 1, 0),
(28, 6, 11, 'sub', 1, 1, 1, 1, 1, 3),
(29, 2, 3, 'main', 1, 1, 1, 1, 1, 2),
(30, 2, 5, 'sub', 1, 1, 1, 1, 1, 0),
(31, 6, 3, 'main', 1, 1, 1, 1, 1, 1),
(32, 6, 2, 'sub', 1, 1, 1, 1, 1, 1),
(33, 6, 9, 'main', 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id_anggota` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `nomor_anggota` varchar(55) NOT NULL,
  `jenis_anggota` int(5) NOT NULL DEFAULT '1' COMMENT '1:anggota\r\n2:pengurus',
  `id_jenis_pengurus` int(15) DEFAULT NULL,
  `nama_lengkap` varchar(75) NOT NULL,
  `nik_ktp` varchar(25) NOT NULL,
  `alamat_tinggal` varchar(128) NOT NULL,
  `rt` varchar(15) NOT NULL,
  `rw` varchar(15) NOT NULL,
  `kecamatan` varchar(25) NOT NULL,
  `kelurahan` varchar(25) NOT NULL,
  `kabupaten` varchar(25) NOT NULL,
  `provinsi` varchar(25) NOT NULL,
  `lokasi_kebun` varchar(128) NOT NULL,
  `luas_kebun` varchar(15) NOT NULL,
  `bukti_kepemilikan` varchar(128) NOT NULL,
  `tahun_tanam` varchar(15) NOT NULL,
  `asal_bibit` varchar(15) NOT NULL,
  `rata_rata_produksi` varchar(15) NOT NULL,
  `nama_koperasi` varchar(45) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `jumlah_kebun` int(5) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_anggota`
--

INSERT INTO `tb_anggota` (`id_anggota`, `user_id`, `nomor_anggota`, `jenis_anggota`, `id_jenis_pengurus`, `nama_lengkap`, `nik_ktp`, `alamat_tinggal`, `rt`, `rw`, `kecamatan`, `kelurahan`, `kabupaten`, `provinsi`, `lokasi_kebun`, `luas_kebun`, `bukti_kepemilikan`, `tahun_tanam`, `asal_bibit`, `rata_rata_produksi`, `nama_koperasi`, `telepon`, `foto`, `jumlah_kebun`, `created_at`) VALUES
(2, 22, '3333333333', 2, 34, 'khuszaimah Azizah', '140705441307779', 'Jln. Hiu', '02', '01', 'Bagan Sinembah Raya', 'Bagan Sinembah', 'Kabupaten Rokan Hilir', 'Riau', 'Jln parit indah', '4 Ha', 'BAB_I-_Sistem_informasi_KTA_dan_Jual_Beli_Kecambah_DPP_APKASINDO_RIAU_(Khuszaimah_azizah).pdf', '2013', 'Sinar Mas', '50 Tons', 'Kelompok Tani Gelora', '085278605999', 'FBGE2031.JPG', 1, '2022-12-08 02:22:11'),
(3, 23, '234334', 2, 34, 'Atiqa Najwa Anggraini', '14070000000', 'Jln.Cucuct', '03', '04', 'Koto Gasib', 'Kuala Gasib', 'Kabupaten S I A K', 'Riau', 'Jln parit indah', '4 Ha', 'penting.pdf', '2013', 'Sinar Mas', '60 Tons', 'Kelompok Tani', '085278605999', 'WhatsApp_Image_2021-02-18_at_14_27_51.jpeg', 1, '2022-12-08 03:20:32'),
(5, 25, '', 1, 18, 'J24bPsJMmg', 'g5cdOeskhS', '7ehKux8odH', 'FLaa7OMUSa', 't73TzJjMo1', 'Puncak Sorik Marapi', 'Huta Baringin', 'Kabupaten Mandailing Nata', 'Sumatera Utara', 'dI63EKnlIk', 'OgBiAbVXxv', 'benar_format.PNG', 'oR4xyOjNZn', 'E2FUDXTbbi', 'ycbCPyf6YZ', 'Luoojb8LdE', '7J7WgIUnNj', 'bagan-alur-kerja-mvc.png', 1, '2022-12-08 19:17:03'),
(6, 27, '1578873767t37t73t73y3828', 2, 34, 'Mahdiawan ', '12333445', 'Jl Dusun Muaro Jayo', '009', '001', 'Lubuk Dalam', 'Lubuk Dalam', 'Kabupaten S I A K', 'Riau', 'Lubuk Dalam', '2000', 'distribusi-sampling.pdf', '2001', 'asira', '10000', 'tenre', '097897878444', 'Perkebunan-Sawit.jpg', 1, '2022-12-13 20:42:06'),
(7, 29, '', 2, 34, 'vmgjPvbvR9', '45623545654', 'QV6aQcueE1', '008', '002', 'Sinunukan', 'Sinunukan I Central', 'Kabupaten Mandailing Nata', 'Sumatera Utara', 'iVYf4By44Q', 'wfvZ5Q2qO5', 'asy.pdf', 'Mk5xcM4C0Y', '4FZAARl6TX', 'scjUJuMd57', 'SrD6VhtQiU', 'eQuQGumwF9', 'F1L10D4IQHIUL3L.png', 1, '2023-02-01 14:51:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota_jenis`
--

CREATE TABLE `tb_anggota_jenis` (
  `id_jenis` int(5) NOT NULL,
  `jenis` varchar(45) NOT NULL,
  `type` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_anggota_jenis`
--

INSERT INTO `tb_anggota_jenis` (`id_jenis`, `jenis`, `type`) VALUES
(18, 'Dewan Pembina DPP', 1),
(19, 'Dewan Penasehat DPP', 1),
(20, 'Dewan Pakar DPP', 1),
(21, 'Pengurus DPP', 1),
(22, 'Dewan Pembina DPW', 1),
(23, 'Dewan Penasehat DPW', 1),
(24, 'Dewan Pakar DPW', 1),
(25, 'Pengurus DPW', 1),
(26, 'Dewan Pembina DPD', 1),
(27, 'Dewan Penasehat DPD', 1),
(28, 'Dewan Pakar DPD', 1),
(29, 'Pengurus DPD', 1),
(30, 'Dewan Pembina DPU', 1),
(31, 'Dewan Penasehat DPU', 1),
(32, 'Dewan Pakar DPU', 1),
(33, 'Pengurus DPU', 1),
(34, 'Non Pengurus', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota_verif`
--

CREATE TABLE `tb_anggota_verif` (
  `id_verif` int(5) NOT NULL,
  `anggota_id` int(5) NOT NULL,
  `status` int(5) NOT NULL COMMENT '0:proses\r\n1:accepted\r\n2:rejected',
  `keterangan` varchar(75) DEFAULT NULL,
  `bukti_bayar` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_pendaftar` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_anggota_verif`
--

INSERT INTO `tb_anggota_verif` (`id_verif`, `anggota_id`, `status`, `keterangan`, `bukti_bayar`, `created_at`, `is_pendaftar`) VALUES
(2, 2, 1, NULL, 'bukti_bayar_1670466168.JPG', NULL, 'Y'),
(3, 3, 1, NULL, 'bukti_bayar_1670469684.jpeg', NULL, 'Y'),
(4, 4, 1, NULL, NULL, NULL, 'N'),
(5, 5, 2, NULL, NULL, NULL, 'N'),
(6, 6, 1, '', 'bukti_bayar_1670966171.png', NULL, 'Y'),
(7, 7, 0, NULL, NULL, NULL, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fileakses`
--

CREATE TABLE `tb_fileakses` (
  `id_fileakses` int(5) NOT NULL,
  `level_id` int(5) NOT NULL,
  `jenis_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_fileakses`
--

INSERT INTO `tb_fileakses` (`id_fileakses`, `level_id`, `jenis_id`) VALUES
(5, 4, 1),
(8, 5, 1),
(3, 3, 2),
(6, 4, 3),
(9, 5, 3),
(2, 3, 4),
(1, 3, 5),
(4, 4, 5),
(7, 5, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_filejenis`
--

CREATE TABLE `tb_filejenis` (
  `id_file` int(5) NOT NULL,
  `nama_file` varchar(45) NOT NULL,
  `keterangan` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_filejenis`
--

INSERT INTO `tb_filejenis` (`id_file`, `nama_file`, `keterangan`) VALUES
(1, 'sp_tanamsendiri', 'Surat Pernyataan ditanam Sendiri'),
(2, 'sp_kecambah', 'Surat Permintaan Kecambah'),
(3, 'surat_tanah', 'Surat Tanah'),
(4, 'sp2bpks', 'SP2BKS'),
(5, 'ktp', 'FOTO KTP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fileupload`
--

CREATE TABLE `tb_fileupload` (
  `id_upload` int(5) NOT NULL,
  `file_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `kd_transaksi` varchar(25) NOT NULL,
  `file` varchar(45) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_fileupload`
--

INSERT INTO `tb_fileupload` (`id_upload`, `file_id`, `user_id`, `kd_transaksi`, `file`, `file_path`, `created_at`) VALUES
(1, 5, 18, 'TRX-0001:18', 'bnk-bw-l-removebg-preview.png', 'D:/_#project/ClientKuzaimah/uploads/nur/', '2022-12-07'),
(2, 1, 18, 'TRX-0001:18', 'file-name.png', 'D:/_#project/ClientKuzaimah/uploads/nur/', '2022-12-07'),
(3, 3, 18, 'TRX-0001:18', 'WhatsApp_Image_2022-11-23_at_09_10_12.jpeg', 'D:/_#project/ClientKuzaimah/uploads/nur/', '2022-12-07'),
(4, 5, 23, 'TRX-0001:23', 'WhatsApp_Image_2021-02-18_at_14_27_511.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '2022-12-14'),
(5, 5, 23, 'TRX-0001:23', 'WhatsApp_Image_2021-02-18_at_14_27_512.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '2022-12-14'),
(6, 5, 23, 'TRX-0001:23', 'WhatsApp_Image_2021-02-18_at_14_27_513.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '2022-12-14'),
(7, 1, 23, 'TRX-0001:23', 'WhatsApp_Image_2021-02-18_at_14_27_514.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '2022-12-14'),
(8, 3, 23, 'TRX-0001:23', 'WhatsApp_Image_2021-02-18_at_14_27_515.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '2022-12-14'),
(9, 5, 27, 'TRX-0001:27', '6b-Distribusi-Sampling2.pdf', 'D:/_#project/ClientKuzaimah/uploads/mahdiawan/', '2022-12-14'),
(10, 1, 27, 'TRX-0001:27', 'Bab_3_-_Distribusi_Sampling1.pdf', 'D:/_#project/ClientKuzaimah/uploads/mahdiawan/', '2022-12-14'),
(11, 3, 27, 'TRX-0001:27', 'distribusi-sampling1.pdf', 'D:/_#project/ClientKuzaimah/uploads/mahdiawan/', '2022-12-14'),
(12, 5, 28, 'TRX-0001:28', 'laporan_flip_flop.pdf', 'D:/_#project/ClientKuzaimah/uploads/wili/', '2022-12-15'),
(13, 1, 28, 'TRX-0001:28', 'Pengenalan-Bahasa-C.pdf', 'D:/_#project/ClientKuzaimah/uploads/wili/', '2022-12-15'),
(14, 3, 28, 'TRX-0001:28', 'Pengenalan-Bahasa-C1.pdf', 'D:/_#project/ClientKuzaimah/uploads/wili/', '2022-12-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_level`
--

CREATE TABLE `tb_level` (
  `id_level` int(5) NOT NULL,
  `nama_level` varchar(25) NOT NULL,
  `aktif` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_level`, `aktif`) VALUES
(1, 'Superadmin', 1),
(2, 'Admin KTA', 1),
(3, 'Penangkar', 1),
(4, 'Anggota', 1),
(5, 'Non Anggota', 1),
(6, 'Admin Transaksi', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id_menu` int(5) NOT NULL,
  `nama_menu` varchar(25) NOT NULL,
  `url` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `nama_menu`, `url`) VALUES
(1, 'Data Master', '#'),
(2, 'Validasi KTA', 'validasi/kta'),
(3, 'Laporan', '#'),
(4, 'User', '#'),
(5, 'Template kartu', 'kartu'),
(6, 'Pengaturan Sistem', '#'),
(7, 'My KTA', 'user/kta'),
(8, 'Transaksi Bibit', 'user/transaksi'),
(9, 'Transaksi Bibit', 'admin/transaksi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembeli`
--

CREATE TABLE `tb_pembeli` (
  `id_pembeli` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `jenis_pembeli` int(5) NOT NULL COMMENT '1:penangkar 2:petani non anggota',
  `nama_lengkap` varchar(45) NOT NULL,
  `nik_ktp` int(45) NOT NULL,
  `alamat_tinggal` varchar(75) NOT NULL,
  `kecamatan` varchar(45) NOT NULL,
  `kabupaten` varchar(25) NOT NULL,
  `provinsi` varchar(25) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `foto` varchar(75) NOT NULL,
  `status` int(5) NOT NULL COMMENT '0:proses\r\n1:setujui\r\n2:tolak',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembeli`
--

INSERT INTO `tb_pembeli` (`id_pembeli`, `user_id`, `jenis_pembeli`, `nama_lengkap`, `nik_ktp`, `alamat_tinggal`, `kecamatan`, `kabupaten`, `provinsi`, `telepon`, `foto`, `status`, `created_at`) VALUES
(1, 28, 1, 'Atiqa Najwa Anggraini', 0, 'Jln.Cucuct', 'Lamba Leda', 'Kabupaten Manggarai Timur', 'Nusa Tenggara Timur', '99999999999999', 'WhatsApp_Image_2021-02-18_at_14_27_51.jpeg', 0, '2022-12-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_submenu`
--

CREATE TABLE `tb_submenu` (
  `id_submenu` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `nama_menu` varchar(25) NOT NULL,
  `url` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_submenu`
--

INSERT INTO `tb_submenu` (`id_submenu`, `menu_id`, `nama_menu`, `url`) VALUES
(1, 1, 'Data Anggota', 'master/anggota'),
(2, 1, 'Data Pembeli', 'master/anggota/pembeli'),
(3, 2, 'Validasi KTA', 'validasi/kta'),
(5, 3, 'Data Anggota', 'admin/laporan'),
(6, 3, 'Data Pembeli', 'admin/laporan/pembeli'),
(7, 4, 'Admin', 'admin/user'),
(8, 4, 'Pengguna', 'admin/user/index/pengguna'),
(9, 6, 'Level User', 'pengaturan/level'),
(10, 6, 'Hak Akses', 'pengaturan/akses'),
(11, 3, 'Transaksi Bibit', 'admin/laporan/transaksi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id_transaksi` int(5) NOT NULL,
  `kode_transaksi` varchar(15) NOT NULL,
  `user_id` int(5) NOT NULL,
  `jumlah_bibit` varchar(128) NOT NULL,
  `total_harga` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id_transaksi`, `kode_transaksi`, `user_id`, `jumlah_bibit`, `total_harga`) VALUES
(2, 'TRX-0001:23', 23, '500', '1,000,000'),
(3, 'TRX-0001:27', 27, '40000', '80,000,000'),
(4, 'TRX-0001:28', 28, '9000', '18,000,000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_invoice`
--

CREATE TABLE `tb_transaksi_invoice` (
  `id_invoice` int(5) NOT NULL,
  `kode_invoice` varchar(15) NOT NULL,
  `kode_transaksi` varchar(15) NOT NULL,
  `nama_rekening` varchar(75) NOT NULL,
  `jumlah_bayar` varchar(15) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') DEFAULT NULL,
  `status_pembayaran` enum('unpaid','proses','success') NOT NULL,
  `status_beli` set('proses','rejected','accepted') NOT NULL DEFAULT 'proses',
  `bukti_pembayaran` varchar(75) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi_invoice`
--

INSERT INTO `tb_transaksi_invoice` (`id_invoice`, `kode_invoice`, `kode_transaksi`, `nama_rekening`, `jumlah_bayar`, `jenis_pembayaran`, `status_pembayaran`, `status_beli`, `bukti_pembayaran`, `file_path`, `keterangan`, `tgl_bayar`, `created_at`) VALUES
(2, '411-779', 'TRX-0001:23', 'bri', 'Rp 1,000,000', 'transfer', 'success', 'accepted', 'buktitranser-1671030191.jpeg', 'D:/_#project/ClientKuzaimah/uploads/Atiqa/', '', '2022-12-14', '2022-12-14 15:02:21'),
(3, '310-741', 'TRX-0001:27', 'mahdiawan nurkholifah', 'Rp 80,000,000', 'transfer', 'success', 'accepted', 'buktitranser-1671055611.png', 'D:/_#project/ClientKuzaimah/uploads/mahdiawan/', '', '2022-12-14', '2022-12-14 22:00:08'),
(4, '120-565', 'TRX-0001:28', 'bri', 'Rp 18,000,000', 'transfer', 'success', 'rejected', 'buktitranser-1671082957.jpeg', 'D:/_#project/ClientKuzaimah/uploads/wili/', '', '2022-12-15', '2022-12-15 05:42:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `level`) VALUES
(1, 'superadmin', 'e10adc3949ba59abbe56e057f20f883e', 1),
(20, 'admin-kta', 'e10adc3949ba59abbe56e057f20f883e', 2),
(21, 'admin-trx', 'e10adc3949ba59abbe56e057f20f883e', 6),
(22, 'Azizah', 'e10adc3949ba59abbe56e057f20f883e', 4),
(23, 'Atiqa', 'e10adc3949ba59abbe56e057f20f883e', 4),
(24, 'tesssa', '202cb962ac59075b964b07152d234b70', 4),
(25, 'D0slw5MuH8', '202cb962ac59075b964b07152d234b70', 4),
(26, 'RgzTGJ9pcX', '00c66aaf5f2c3f49946f15c1ad2ea0d3', 4),
(27, 'mahdiawan', '202cb962ac59075b964b07152d234b70', 4),
(28, 'wili', 'e10adc3949ba59abbe56e057f20f883e', 5),
(29, 'tessss', '202cb962ac59075b964b07152d234b70', 4);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_anggota_aktif`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_anggota_aktif` (
`id_anggota` int(5)
,`user_id` int(5)
,`nomor_anggota` varchar(55)
,`jenis_anggota` int(5)
,`nama_lengkap` varchar(75)
,`nik_ktp` varchar(25)
,`alamat_tinggal` varchar(128)
,`rt` varchar(15)
,`rw` varchar(15)
,`kelurahan` varchar(25)
,`kecamatan` varchar(25)
,`kabupaten` varchar(25)
,`provinsi` varchar(25)
,`lokasi_kebun` varchar(128)
,`luas_kebun` varchar(15)
,`bukti_kepemilikan` varchar(128)
,`tahun_tanam` varchar(15)
,`asal_bibit` varchar(15)
,`rata_rata_produksi` varchar(15)
,`nama_koperasi` varchar(45)
,`telepon` varchar(15)
,`foto` varchar(128)
,`jumlah_kebun` int(5)
,`created_at` datetime
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_file_akses`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_file_akses` (
`id_fileakses` int(5)
,`level_id` int(5)
,`jenis_id` int(5)
,`id_file` int(5)
,`nama_file` varchar(45)
,`keterangan` varchar(75)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_file_transaksi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_file_transaksi` (
`id_file` int(5)
,`nama_file` varchar(45)
,`keterangan` varchar(75)
,`id_upload` int(5)
,`file_id` int(5)
,`user_id` int(5)
,`kd_transaksi` varchar(25)
,`file` varchar(45)
,`file_path` varchar(128)
,`created_at` date
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_main_menu`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_main_menu` (
`menu_id` int(5)
,`nama_menu` varchar(25)
,`url` varchar(55)
,`id_level` int(5)
,`nama_level` varchar(25)
,`urut` int(5)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_new_anggota`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_new_anggota` (
`id_anggota` int(5)
,`user_id` int(5)
,`jenis_anggota` int(5)
,`nomor_anggota` varchar(55)
,`nama_lengkap` varchar(75)
,`nik_ktp` varchar(25)
,`alamat_tinggal` varchar(128)
,`rt` varchar(15)
,`rw` varchar(15)
,`kelurahan` varchar(25)
,`kecamatan` varchar(25)
,`kabupaten` varchar(25)
,`provinsi` varchar(25)
,`lokasi_kebun` varchar(128)
,`luas_kebun` varchar(15)
,`bukti_kepemilikan` varchar(128)
,`tahun_tanam` varchar(15)
,`asal_bibit` varchar(15)
,`rata_rata_produksi` varchar(15)
,`nama_koperasi` varchar(45)
,`telepon` varchar(15)
,`foto` varchar(128)
,`jumlah_kebun` int(5)
,`created_at` date
,`status` int(5)
,`bukti` varchar(45)
,`jenis` varchar(45)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_pembeli_transaksi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_pembeli_transaksi` (
`user_id` int(5)
,`nama_lengkap` varchar(75)
,`jenis` varchar(8)
,`nik_ktp` varchar(25)
,`alamat_tinggal` varchar(128)
,`kecamatan` varchar(25)
,`kabupaten` varchar(25)
,`provinsi` varchar(25)
,`telepon` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_sub_main`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_sub_main` (
`id_submenu` int(5)
,`id_level` int(5)
,`nama_level` varchar(25)
,`menu_id` int(5)
,`nama_menu` varchar(25)
,`url` varchar(55)
,`c` int(5)
,`r` int(5)
,`u` int(5)
,`d` int(5)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_transaksi_bibit`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_transaksi_bibit` (
`id_transaksi` int(5)
,`kode_transaksi` varchar(15)
,`user_id` int(5)
,`jumlah_bibit` varchar(128)
,`total_harga` varchar(15)
,`kode_invoice` varchar(15)
,`jumlah_bayar` varchar(15)
,`nama_rekening` varchar(75)
,`jenis_pembayaran` enum('cash','transfer')
,`tgl_bayar` date
,`status_pembayaran` enum('unpaid','proses','success')
,`bukti_pembayaran` varchar(75)
,`file_path` varchar(128)
,`created_at` date
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_verif_anggota`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_verif_anggota` (
`user_id` int(5)
,`nama_lengkap` varchar(75)
,`id_verif` int(5)
,`anggota_id` int(5)
,`status` int(5)
,`keterangan` varchar(75)
,`bukti_bayar` varchar(45)
,`created_at` datetime
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_anggota_aktif`
--
DROP TABLE IF EXISTS `v_anggota_aktif`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_anggota_aktif`  AS SELECT `a`.`id_anggota` AS `id_anggota`, `a`.`user_id` AS `user_id`, `a`.`nomor_anggota` AS `nomor_anggota`, `a`.`jenis_anggota` AS `jenis_anggota`, `a`.`nama_lengkap` AS `nama_lengkap`, `a`.`nik_ktp` AS `nik_ktp`, `a`.`alamat_tinggal` AS `alamat_tinggal`, `a`.`rt` AS `rt`, `a`.`rw` AS `rw`, `a`.`kelurahan` AS `kelurahan`, `a`.`kecamatan` AS `kecamatan`, `a`.`kabupaten` AS `kabupaten`, `a`.`provinsi` AS `provinsi`, `a`.`lokasi_kebun` AS `lokasi_kebun`, `a`.`luas_kebun` AS `luas_kebun`, `a`.`bukti_kepemilikan` AS `bukti_kepemilikan`, `a`.`tahun_tanam` AS `tahun_tanam`, `a`.`asal_bibit` AS `asal_bibit`, `a`.`rata_rata_produksi` AS `rata_rata_produksi`, `a`.`nama_koperasi` AS `nama_koperasi`, `a`.`telepon` AS `telepon`, `a`.`foto` AS `foto`, `a`.`jumlah_kebun` AS `jumlah_kebun`, `a`.`created_at` AS `created_at` FROM (`tb_anggota` `a` join `tb_anggota_verif` `b` on((`a`.`id_anggota` = `b`.`anggota_id`))) WHERE (`b`.`status` = 1) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_file_akses`
--
DROP TABLE IF EXISTS `v_file_akses`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_file_akses`  AS SELECT `a`.`id_fileakses` AS `id_fileakses`, `a`.`level_id` AS `level_id`, `a`.`jenis_id` AS `jenis_id`, `b`.`id_file` AS `id_file`, `b`.`nama_file` AS `nama_file`, `b`.`keterangan` AS `keterangan` FROM (`tb_fileakses` `a` join `tb_filejenis` `b` on((`a`.`jenis_id` = `b`.`id_file`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_file_transaksi`
--
DROP TABLE IF EXISTS `v_file_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_file_transaksi`  AS SELECT `a`.`id_file` AS `id_file`, `a`.`nama_file` AS `nama_file`, `a`.`keterangan` AS `keterangan`, `b`.`id_upload` AS `id_upload`, `b`.`file_id` AS `file_id`, `b`.`user_id` AS `user_id`, `b`.`kd_transaksi` AS `kd_transaksi`, `b`.`file` AS `file`, `b`.`file_path` AS `file_path`, `b`.`created_at` AS `created_at` FROM (`tb_filejenis` `a` join `tb_fileupload` `b` on((`a`.`id_file` = `b`.`file_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_main_menu`
--
DROP TABLE IF EXISTS `v_main_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_main_menu`  AS SELECT `a`.`menu_id` AS `menu_id`, `b`.`nama_menu` AS `nama_menu`, `b`.`url` AS `url`, `c`.`id_level` AS `id_level`, `c`.`nama_level` AS `nama_level`, `a`.`urut` AS `urut` FROM ((`tb_akses` `a` join `tb_menu` `b` on((`a`.`menu_id` = `b`.`id_menu`))) join `tb_level` `c` on((`a`.`level` = `c`.`id_level`))) WHERE ((`a`.`type_menu` = 'main') AND (`a`.`akses` = 1)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_new_anggota`
--
DROP TABLE IF EXISTS `v_new_anggota`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_new_anggota`  AS SELECT `a`.`id_anggota` AS `id_anggota`, `a`.`user_id` AS `user_id`, `a`.`jenis_anggota` AS `jenis_anggota`, `a`.`nomor_anggota` AS `nomor_anggota`, `a`.`nama_lengkap` AS `nama_lengkap`, `a`.`nik_ktp` AS `nik_ktp`, `a`.`alamat_tinggal` AS `alamat_tinggal`, `a`.`rt` AS `rt`, `a`.`rw` AS `rw`, `a`.`kelurahan` AS `kelurahan`, `a`.`kecamatan` AS `kecamatan`, `a`.`kabupaten` AS `kabupaten`, `a`.`provinsi` AS `provinsi`, `a`.`lokasi_kebun` AS `lokasi_kebun`, `a`.`luas_kebun` AS `luas_kebun`, `a`.`bukti_kepemilikan` AS `bukti_kepemilikan`, `a`.`tahun_tanam` AS `tahun_tanam`, `a`.`asal_bibit` AS `asal_bibit`, `a`.`rata_rata_produksi` AS `rata_rata_produksi`, `a`.`nama_koperasi` AS `nama_koperasi`, `a`.`telepon` AS `telepon`, `a`.`foto` AS `foto`, `a`.`jumlah_kebun` AS `jumlah_kebun`, cast(`a`.`created_at` as date) AS `created_at`, `b`.`status` AS `status`, `b`.`bukti_bayar` AS `bukti`, `c`.`jenis` AS `jenis` FROM ((`tb_anggota` `a` join `tb_anggota_verif` `b` on((`a`.`id_anggota` = `b`.`anggota_id`))) join `tb_anggota_jenis` `c` on((`a`.`id_jenis_pengurus` = `c`.`id_jenis`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_pembeli_transaksi`
--
DROP TABLE IF EXISTS `v_pembeli_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pembeli_transaksi`  AS SELECT `a`.`user_id` AS `user_id`, `a`.`nama_lengkap` AS `nama_lengkap`, (case when (`a`.`jenis_anggota` = 1) then 'Anggota' else 'Pengurus' end) AS `jenis`, `a`.`nik_ktp` AS `nik_ktp`, `a`.`alamat_tinggal` AS `alamat_tinggal`, `a`.`kecamatan` AS `kecamatan`, `a`.`kabupaten` AS `kabupaten`, `a`.`provinsi` AS `provinsi`, `a`.`telepon` AS `telepon` FROM (`tb_anggota` `a` join `tb_anggota_verif` `b`) WHERE ((`a`.`id_anggota` = `b`.`anggota_id`) AND (`b`.`status` = 1)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_sub_main`
--
DROP TABLE IF EXISTS `v_sub_main`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sub_main`  AS SELECT `b`.`id_submenu` AS `id_submenu`, `c`.`id_level` AS `id_level`, `c`.`nama_level` AS `nama_level`, `b`.`menu_id` AS `menu_id`, `b`.`nama_menu` AS `nama_menu`, `b`.`url` AS `url`, `a`.`c` AS `c`, `a`.`r` AS `r`, `a`.`u` AS `u`, `a`.`d` AS `d` FROM ((`tb_akses` `a` join `tb_submenu` `b` on((`a`.`menu_id` = `b`.`id_submenu`))) join `tb_level` `c` on((`a`.`level` = `c`.`id_level`))) WHERE ((`a`.`type_menu` = 'sub') AND (`a`.`akses` = 1)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_transaksi_bibit`
--
DROP TABLE IF EXISTS `v_transaksi_bibit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_transaksi_bibit`  AS SELECT `a`.`id_transaksi` AS `id_transaksi`, `a`.`kode_transaksi` AS `kode_transaksi`, `a`.`user_id` AS `user_id`, `a`.`jumlah_bibit` AS `jumlah_bibit`, `a`.`total_harga` AS `total_harga`, `b`.`kode_invoice` AS `kode_invoice`, `b`.`jumlah_bayar` AS `jumlah_bayar`, `b`.`nama_rekening` AS `nama_rekening`, `b`.`jenis_pembayaran` AS `jenis_pembayaran`, `b`.`tgl_bayar` AS `tgl_bayar`, `b`.`status_pembayaran` AS `status_pembayaran`, `b`.`bukti_pembayaran` AS `bukti_pembayaran`, `b`.`file_path` AS `file_path`, cast(`b`.`created_at` as date) AS `created_at` FROM (`tb_transaksi` `a` join `tb_transaksi_invoice` `b` on((`a`.`kode_transaksi` = `b`.`kode_transaksi`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_verif_anggota`
--
DROP TABLE IF EXISTS `v_verif_anggota`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_verif_anggota`  AS SELECT `a`.`user_id` AS `user_id`, `a`.`nama_lengkap` AS `nama_lengkap`, `b`.`id_verif` AS `id_verif`, `b`.`anggota_id` AS `anggota_id`, `b`.`status` AS `status`, `b`.`keterangan` AS `keterangan`, `b`.`bukti_bayar` AS `bukti_bayar`, `b`.`created_at` AS `created_at` FROM (`tb_anggota` `a` join `tb_anggota_verif` `b` on((`a`.`id_anggota` = `b`.`anggota_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_akses`
--
ALTER TABLE `tb_akses`
  ADD PRIMARY KEY (`id_akses`),
  ADD KEY `level` (`level`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indeks untuk tabel `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indeks untuk tabel `tb_anggota_jenis`
--
ALTER TABLE `tb_anggota_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `tb_anggota_verif`
--
ALTER TABLE `tb_anggota_verif`
  ADD PRIMARY KEY (`id_verif`);

--
-- Indeks untuk tabel `tb_fileakses`
--
ALTER TABLE `tb_fileakses`
  ADD PRIMARY KEY (`id_fileakses`),
  ADD KEY `jenis_id` (`jenis_id`,`level_id`),
  ADD KEY `level_id` (`level_id`);

--
-- Indeks untuk tabel `tb_filejenis`
--
ALTER TABLE `tb_filejenis`
  ADD PRIMARY KEY (`id_file`);

--
-- Indeks untuk tabel `tb_fileupload`
--
ALTER TABLE `tb_fileupload`
  ADD PRIMARY KEY (`id_upload`);

--
-- Indeks untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `tb_pembeli`
--
ALTER TABLE `tb_pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indeks untuk tabel `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id_submenu`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `tb_transaksi_invoice`
--
ALTER TABLE `tb_transaksi_invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `level` (`level`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_akses`
--
ALTER TABLE `tb_akses`
  MODIFY `id_akses` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id_anggota` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_anggota_jenis`
--
ALTER TABLE `tb_anggota_jenis`
  MODIFY `id_jenis` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `tb_anggota_verif`
--
ALTER TABLE `tb_anggota_verif`
  MODIFY `id_verif` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_fileakses`
--
ALTER TABLE `tb_fileakses`
  MODIFY `id_fileakses` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_filejenis`
--
ALTER TABLE `tb_filejenis`
  MODIFY `id_file` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_fileupload`
--
ALTER TABLE `tb_fileupload`
  MODIFY `id_upload` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id_level` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id_menu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_pembeli`
--
ALTER TABLE `tb_pembeli`
  MODIFY `id_pembeli` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_submenu`
--
ALTER TABLE `tb_submenu`
  MODIFY `id_submenu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id_transaksi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi_invoice`
--
ALTER TABLE `tb_transaksi_invoice`
  MODIFY `id_invoice` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_akses`
--
ALTER TABLE `tb_akses`
  ADD CONSTRAINT `tb_akses_ibfk_1` FOREIGN KEY (`level`) REFERENCES `tb_level` (`id_level`);

--
-- Ketidakleluasaan untuk tabel `tb_fileakses`
--
ALTER TABLE `tb_fileakses`
  ADD CONSTRAINT `tb_fileakses_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `tb_level` (`id_level`),
  ADD CONSTRAINT `tb_fileakses_ibfk_2` FOREIGN KEY (`jenis_id`) REFERENCES `tb_filejenis` (`id_file`);

--
-- Ketidakleluasaan untuk tabel `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD CONSTRAINT `tb_submenu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `tb_menu` (`id_menu`);

--
-- Ketidakleluasaan untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`level`) REFERENCES `tb_level` (`id_level`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
