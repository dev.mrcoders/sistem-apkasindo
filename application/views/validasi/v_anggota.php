<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead id="hd">
                                <tr>
                                    <th>#</th>
                                    <th>Foto</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($data as $i=>$d): ?>
                                    <?php $path = $this->db->get_where('tb_user', ['id_user'=>$d['user_id']])->row()->username?>
                                <tr>
                                    <td>
                                        <?=$i+1?>
                                    </td>
                                    <td>
                                        <img style="border-radius:0px" src="<?=base_url()?>uploads/<?=$path?>/<?=$d['foto']?>"
                                            alt="">
                                    </td>
                                    <td>
                                        <p>Nama : <?=$d['nama_lengkap']?></p>
                                        <p>Asal wilayah dari kabupaten <?=$d['kabupaten']?> desa <?=$d['kecamatan']?>
                                        </p>
                                    </td>
                                    <td>
                                        <?php if ($d['status'] == 0): ?>
                                            <label for="" class="badge badge-info">Proses</label>
                                        <?php elseif($d['status'] == 1): ?>
                                            <label for="" class="badge badge-success">Setujui</label>
                                        <?php else: ?>
                                            <label for="" class="badge badge-danger">Tolak</label>
                                        <?php endif ?>
                                        
                                            
                                    </td>
                                    <td>
                                        <a href="<?=base_url()?>validasi/kta/detailkta/<?=$d['id_anggota']?>" class="btn btn-warning btn-sm" >Verifikasi</a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php foreach($data as $m): ?>
<div class="modal fade " id="view-<?=$m['id_anggota']?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Anggota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <img src="<?=base_url()?>uploads/<?=$m['foto']?>" class="rounded" style="width:35%" alt="...">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <?php foreach($form as $i=> $f): ?>
                            
                            <div class="col-sm-6">
                                <div class="form-group row" style="margin-bottom:0;"
                                    <?=($i==14? 'hidden':'')?>>
                                    <label for="exampleInputUsername2"
                                        class="col-sm-4 col-form-label"><?=$f->label_name?></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext" id="exampleInputUsername2"
                                            value=":&emsp; <?=$m[$f->name]?>">
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>

<script>
$(document).ready(function() {
    $('#myTable').DataTable({
        "lengthChange": false,
        "drawCallback": function(settings) {
            // $("#myTable thead").remove();
            // $("#myTable tfoot").remove();
        }
    });

    $('.view').on('click', function() {
        let id = $(this).data('id');
        $('#view').modal('show');
    })
});
</script>