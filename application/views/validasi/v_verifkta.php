<style>
    img.barcode-image {
        width: auto;
        height: 65%;
    }

    @media (max-width: 575.98px) {
        .modal-fullscreen {
            padding: 0 !important;
        }

        .modal-fullscreen .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 767.98px) {
        .modal-fullscreen-sm {
            padding: 0 !important;
        }

        .modal-fullscreen-sm .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-sm .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-sm .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 991.98px) {
        .modal-fullscreen-md {
            padding: 0 !important;
        }

        .modal-fullscreen-md .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-md .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-md .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 1199.98px) {
        .modal-fullscreen-lg {
            padding: 0 !important;
        }

        .modal-fullscreen-lg .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-lg .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-lg .modal-body {
            overflow-y: auto;
        }
    }

    .modal-fullscreen-xl {
        padding: 0 !important;
    }

    .modal-fullscreen-xl .modal-dialog {
        width: 100%;
        max-width: none;
        height: auto;
        margin: 0;
    }

    .modal-fullscreen-xl .modal-content {
        height: auto;
        border: 0;
        border-radius: 0;
    }

    .modal-fullscreen-xl .modal-body {
        overflow-y: auto;
    }
</style>
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Identitas Pendaftar</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row m-0">
                        <label class="col-sm-3 col-form-label">Jenis Keanggotan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="" readonly="" value="<?=$data['jenis']?>">
                        </div>
                    </div>
                    <?php foreach ($form as $i=> $f): ?>
                    <?php if ($i != 11 && $i != 17): ?>
                    <div class="form-group row m-0">
                        <label class="col-sm-3 col-form-label"><?=$f->label_name?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="" readonly="" value="<?=$data[$f->name]?>">
                        </div>
                    </div>
                    <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>File Uploads Pendaftar</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Jenis File</td>
                                    <td>File</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $Path = $this->db->get_where('tb_user', ['id_user'=>$data['user_id']])->row()->username; ?>
                                <tr>
                                    <td>Foto Profil</td>
                                    <td>
                                        <a href="#" data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['foto']?>"
                                            class="v-file"><?=$data['foto']?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bukti Kepemilikan</td>
                                    <td>

                                        <a href="#"
                                            data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['bukti_kepemilikan']?>"
                                            class="v-file"><?=$data['bukti_kepemilikan']?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bukti Pembayaran Pendaftaran</td>
                                    <td>
                                        <?php if ($data['bukti']==''): ?>
                                        <label for="" class="badge badge-danger">NoUpload</label>
                                        <?php else: ?>
                                        <a href="#" data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['bukti']?>"
                                            class="v-file"><?=$data['bukti']?></a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Verifikasi Pendaftaran Anggota</h4>
                </div>
                <form method="POST" action="<?=base_url()?>validasi/kta/verifikasikta">
                    <input type="hidden" name="id_anggota" value="<?=$data['id_anggota']?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input verif" name="verifikasi"
                                                id="optionsRadios1" value="1" required>
                                            Setujui
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input verif" name="verifikasi"
                                                id="optionsRadios1" value="2" required>
                                            Tolak
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 in-noanggota" style="display: none;">
                                <label for="">No Anggota</label>
                                <input type="text" class="form-control" name="no_anggota" id="no-anggota">
                                <button type="button" class="btn btn-sm btn-primary generate">Generata barcode & Show
                                    Card ID</button>
                            </div>
                            <div class="form-group col-sm-12 in-alasan" style="display: none;">
                                <label for="">Alasan Penolakan</label>
                                <textarea name="alasan" id="alasan" rows="5" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <img src="" id="kta-temp" class="w-100">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <a href="<?=base_url()?>validasi/kta" class="btn btn-sm btn-default">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary sbmt" disabled="">Submit Data</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="view-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="" alt="" id="img-view" class="img-thumbnail rounded mx-auto d-block">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-fullscreen-xl" id="modal-fullscreen-xl" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="createImg">
                    <div class="col-sm-6">
                        <div class="card">
                            <div>
                                <img class="card-img  mx-auto" src="<?=base_url()?>assets/front-kta.jpeg" alt="Sheep 9"
                                    style="height: 361px;">
                            </div>
                            <div class="card-img-overlay ">
                                <img src="<?=base_url()?>uploads/<?=$Path?>/<?=$data['foto']?>"
                                    class="rounded card-img w-25 " alt="..."
                                    style="height: 50%;margin: 9.5rem 0 0 2.5rem">
                            </div>
                            <div class="card-img-overlay text-white ">
                                <div class="row">
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-7 " style="color:black;margin-top:75px">
                                        <div class="text-center title font-weight-bold ml-2 pt-4">
                                            <?=$data['nama_lengkap']?></div>
                                        <div class="text-left ml-3 ">
                                            <label for="" id="noanggota" class="font-weight-bold mt-4 mb-2"
                                                style="font-size: 18px;">---</label>
                                            <div><?=$data['alamat_tinggal']?></div>
                                            <div>RT <?=$data['rt']?> RW <?=$data['rw']?></div>
                                            <div>Kel <?=$data['kelurahan']?></div>
                                            <div>Kec <?=$data['kecamatan']?></div>
                                            <div>Kab <?=$data['kabupaten']?></div>
                                            <div class="">provinsi <?=$data['provinsi']?></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class=" mt-4" style="background: white;font-size: 12px;">
                                                        Berlaku s/d 31/12/2024
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <img src="" class="barcode-image" alt="" id="barcode">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <img class="card-img" src="<?=base_url()?>assets/back-kta.jpeg" style="height: 361px;"
                                alt="Sheep 9">
                            <div class="card-img-overlay text-white "></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary generated">Generate Card ID</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".verif").click(function () {
            let formDta = $('#form-id').serializeArray();

            var radioValue = $(".verif:checked").val();
            DetailKartu(formDta)
            if (radioValue == 2) {
                $('.in-alasan').css('display', 'block');
                $('.in-noanggota').css('display', 'none');
                $('#alasan').attr('required', true);
                $('#no-anggota').attr('required', false);
                $('#no-anggota').val('');
            } else {
                $('#template-kta').removeAttr('hidden');
                $('.in-alasan').css('display', 'none');
                $('.in-noanggota').css('display', 'block');
                $('#alasan').attr('required', false);
                $('#no-anggota').attr('required', true);
                $('#alasan').val('');
            }
        })

        $("#no-anggota").keyup(function (event) {
            text = $(this).val();
            if (text == '') {
                $("#noanggota").text('-------');
            } else {
                $("#noanggota").text(text);
                $('.sbmt').removeAttr('disabled');
            }

        });
        $('#alasan').keyup(function (argument) {
            text = $(this).val();
            console.log(text);
            if (text != '') {
                $('.sbmt').removeAttr('disabled');
            } else {
                $('.sbmt').attr('disabled', true);
            }
        })

        $('.generate').on('click', function (event) {
            event.preventDefault();
            text = $('#no-anggota').val();
            if (text == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No Anggota tidak boleh kosong',
                }).then((result) => {
                    if (result.isConfirmed) {

                    }

                })
                return false;
            }
            $('#noanggota').text(text);
            $('#modal-fullscreen-xl').modal('show');
            $.ajax({
                url: '<?=base_url()?>welcome/createbarcode',
                type: 'GET',
                data: {
                    text: text
                },
                success: function (data) {
                    $('#barcode').attr('src', data);
                }
            });

            $('.generated').on('click', function (event) {
                event.preventDefault();
                text = $('#no-anggota').val();
                $('#noanggota').text(text);

                GenerateCard(text);
                $('#modal-fullscreen-xl').modal('hide');

            });
        });



        $('.v-file').click(function (event) {
            let file = $(this).data('file');
            $('#img-view').attr('src', file);
            $('#view-file').modal('show');
        });

        function DetailKartu(data) {
            $.each(data, function (index, val) {
                if (val.name == 'no_anggota')
                    $('#no-anggota').text(val.value);

            });
        }

        function GenerateCard(nokta) {
            html2canvas($("#createImg"), {
                onrendered: function (canvas) {
                    var imgsrc = canvas.toDataURL("image/png", 1.0);

                    $('#kta-temp').attr('src', imgsrc);
                    var dataURL = canvas.toDataURL();
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>welcome/savekta",
                        data: {
                            imgBase64: dataURL,
                            noanggota: nokta
                        },
                        success: function (a) {

                        }
                    })
                }
            });
        }
    });
</script>