<style>
    img.barcode-image {
        width: auto;
        height: 65%;
    }
    @media (max-width: 575.98px) {
      .modal-fullscreen {
        padding: 0 !important;
    }
    .modal-fullscreen .modal-dialog {
        width: 100%;
        max-width: none;
        height: 100%;
        margin: 0;
    }
    .modal-fullscreen .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0;
    }
    .modal-fullscreen .modal-body {
        overflow-y: auto;
    }
}
@media (max-width: 767.98px) {
  .modal-fullscreen-sm {
    padding: 0 !important;
}
.modal-fullscreen-sm .modal-dialog {
    width: 100%;
    max-width: none;
    height: 100%;
    margin: 0;
}
.modal-fullscreen-sm .modal-content {
    height: 100%;
    border: 0;
    border-radius: 0;
}
.modal-fullscreen-sm .modal-body {
    overflow-y: auto;
}
}
@media (max-width: 991.98px) {
  .modal-fullscreen-md {
    padding: 0 !important;
}
.modal-fullscreen-md .modal-dialog {
    width: 100%;
    max-width: none;
    height: 100%;
    margin: 0;
}
.modal-fullscreen-md .modal-content {
    height: 100%;
    border: 0;
    border-radius: 0;
}
.modal-fullscreen-md .modal-body {
    overflow-y: auto;
}
}
@media (max-width: 1199.98px) {
  .modal-fullscreen-lg {
    padding: 0 !important;
}
.modal-fullscreen-lg .modal-dialog {
    width: 100%;
    max-width: none;
    height: 100%;
    margin: 0;
}
.modal-fullscreen-lg .modal-content {
    height: 100%;
    border: 0;
    border-radius: 0;
}
.modal-fullscreen-lg .modal-body {
    overflow-y: auto;
}
}
.modal-fullscreen-xl {
  padding: 0 !important;
}
.modal-fullscreen-xl .modal-dialog {
  width: 100%;
  max-width: none;
  height: auto;
  margin: 0;
}
.modal-fullscreen-xl .modal-content {
    height: auto;
    border: 0;
    border-radius: 0;
}
.modal-fullscreen-xl .modal-body {
  overflow-y: auto;
}
</style>
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <form action="<?=base_url()?>master/anggota/storeanggota" method="POST" enctype="multipart/form-data" id="form-sub">
                <div class="card">
                    <div class="card-header">
                        <h4>Identitas Pendaftar</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                              <div class="form-example-int form-horizental mg-t-15">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                      <label class="hrzn-fm">Jenis Keanggotaan</label>
                                  </div>
                                  <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                      <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                          <input type="radio" name="jenisanggota" value="1" class="selectgroup-input" checked="" required>
                                          <span class="selectgroup-button">Pengurus</span>
                                      </label>
                                      <label class="selectgroup-item">
                                          <input type="radio" name="jenisanggota" value="2" class="selectgroup-input" required>
                                          <span class="selectgroup-button">Anggota</span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="">No Anggota</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="no_anggota" id="no-anggota" required>
            </div>
        </div>

        <?php foreach($form as $fm): ?>
            <div class="row mb-2">
              <label for="" class="col-sm-3"><?=$fm->label_name?></label>
              <div class="col-sm-9">
                <?php if ($fm->name == 'provinsi' || $fm->name == 'kabupaten' || $fm->name == 'kecamatan' || $fm->name == 'kelurahan'): ?>
                  <select name="<?=$fm->name?>" id="<?=$fm->name?>" class="form-control" required>
                    <option>Pilih</option>
                </select>
            <?php else: ?>
                <?php if ($fm->name == 'foto'): ?>
                    <input type="<?=$fm->type?>" onchange="readURL(this);" id="n-upload"  class="form-control required" name="<?=$fm->name?>" value="" required>
                <?php else: ?>
                    <input type="<?=$fm->type?>" name="<?=$fm->name?>" class="form-control required"  value="" required>
                <?php endif ?>

            <?php endif ?>
        </div>
    </div>
<?php endforeach ?>

</div>
</div>
<div class="card">
    <div class="card-header">
        <h4>Creating Account User</h4>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="">Username</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="username"  required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="">Password</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="password" required>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h4>Template Kartu</h4>
    </div>


    <div class="card-body">
        <button type="button" class="btn btn-sm btn-primary generate btn-block">Generata barcode & Show Card ID</button>
        <div class="row">
            <div class="col-sm-12">
                <img src="" id="kta-temp" class="w-100">
            </div>
        </div>
    </div>
    <div class="card-footer text-right">
        <a href="<?=base_url()?>master/anggota" class="btn btn-sm btn-default">Cancel</a>
        <button type="submit" class="btn btn-sm btn-primary submited" disabled="">Submit Data</button>

    </div>
</div>
</form>
</div>
</div>
</div>


<div class="modal modal-fullscreen-xl" id="modal-fullscreen-xl" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="createImg">
                    <div class="col-sm-6" >
                        <div class="card" >
                            <div>
                                <img class="card-img  mx-auto" src="<?=base_url()?>assets/front-kta.jpeg" alt="Sheep 9" style="height: 361px;">
                            </div>
                            <div class="card-img-overlay ">
                                <img src="" id="show-img" class="rounded card-img w-25 " alt="..." style="height: 50%;margin: 9.5rem 0 0 2.5rem">
                            </div>
                            <div class="card-img-overlay text-white ">
                                <div class="row">
                                    <div class="col-sm-5" ></div>
                                    <div class="col-sm-7 " style="color:black;margin-top:75px">
                                        <div class="text-center title font-weight-bold ml-2" style="font-size: 18px;height: 40px;" id="name-anggota"></div>
                                        <div class="text-left ml-2 ">
                                            <label for="" id="noanggota" class="font-weight-bold mt-5 mb-2" style="font-size: 18px;">---</label>
                                            <div id="alamat-anggota"></div>
                                            <div id="rtrw-anggota"> </div>
                                            <div id="kel-anggota">Kel </div>
                                            <div id="kec-anggota">Kec </div>
                                            <div id="kab-anggota">Kab </div>
                                            <div class="" id="prov-anggota">provinsi </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class=" mt-4" style="background: white;font-size: 12px;">Berlaku s/d 31/12/2024
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <img src="" class="barcode-image" alt="" id="barcode" >
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <img class="card-img" src="<?=base_url()?>assets/back-kta.jpeg" style="height: 361px;" alt="Sheep 9">
                            <div class="card-img-overlay text-white "></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary generated" >Generate Card ID</button>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#show-img')
            .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function() {
    $('.submited').attr('disabled',true);
    $('#n-upload').on('change', function () {
      readURL(input);
  });

    $("#form-sub").submit(function(){
        var isFormValid = true;

        if($('#kta-temp').attr('src') == ''){
            isFormValid = false;
        }else{
            isFormValid = true;
        }

        if (!isFormValid) alert("Silahkan Generate Card ID Terlebih Dahulu");

        return isFormValid;
    });


    $(".verif").click(function(){
        let formDta = $('#form-id').serializeArray();

        var radioValue = $(".verif:checked").val();
        DetailKartu(formDta)
        if(radioValue == 2){
            $('.in-alasan').css('display', 'block');
            $('.in-noanggota').css('display','none');
        }else{

            $('#template-kta').removeAttr('hidden');
            $('.in-alasan').css('display','none');
            $('.in-noanggota').css('display','block');
        }
    })

    $("#no-anggota").keyup(function(event) {
      text = $(this).val();
      if (text == '') {
        $("#noanggota").text('-------');
    }else{
        $("#noanggota").text(text);
    }

});

    $('.generate').on('click',function(event) {
        event.preventDefault();
        text = $('#no-anggota').val();
        if(text == ''){
           return alert('Nomor Anggota Tidak Boleh Kosong');
       }

       let formD=$('#form-sub').serializeArray(),dataObj={};

       $(formD).each(function(i, field){
          dataObj[field.name] = field.value;
      });


       $('#name-anggota').html(dataObj['nama_lengkap']);
       $('#alamat-anggota').html(dataObj['alamat_tinggal']);
       $('#rtrw-anggota').html('RT '+dataObj['rt']+' RW '+dataObj['rw']);
       $('#kel-anggota').html('Kel '+dataObj['kelurahan']);
       $('#kec-anggota').html('Kec '+dataObj['kecamatan']);
       $('#kab-anggota').html('Kab '+dataObj['kabupaten']);
       $('#prov-anggota').html('Provinsi '+dataObj['provinsi']);
       $('#noanggota').text(text);
       $('#modal-fullscreen-xl').modal('show');
       $.ajax({
        url: '<?=base_url()?>welcome/createbarcode',
        type: 'GET',
        data: {text:text},
        success: function (data) {
            $('#barcode').attr('src', data);
        }
    });
   });




    $('.generated').on('click',function(event) {
        event.preventDefault();
        text = $('#no-anggota').val();
        $('#noanggota').text(text);

        GenerateCard(text);
        $('#modal-fullscreen-xl').modal('hide');
        $('.submited').attr('disabled',false);

    });



    $('.v-file').click(function(event) {
        let file = $(this).data('file');
        $('#img-view').attr('src', file);
        $('#view-file').modal('show');
    });

    function DetailKartu(data) {
       $.each(data, function(index, val) {
        if(val.name == 'no_anggota')
            $('#no-anggota').text(val.value);

    });
   }

   function GenerateCard(nokta){
    html2canvas($("#createImg"), {
        onrendered: function(canvas) {
            var imgsrc = canvas.toDataURL("image/png",1.0);

            $('#kta-temp').attr('src', imgsrc);
            var dataURL = canvas.toDataURL();
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>welcome/savekta",
                data: {
                    imgBase64: dataURL,noanggota:nokta
                },
                success:function(a) {

                }
            })
        }
    });
}
});
$(function() {
    let provinsi='';
    let kabupaten='';
    let kecamatan='';
    let kelurahan='';

    $('#nik_ktp').attr('onkeypress', 'return onlyNumberKey(event)');

    $.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi', function(data) {
      provinsi +='<option value="">Pilih Provinsi</option>';
      $.each(data, function(index, val) {
        $.each(val,function(index, el) {
          provinsi +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
      });
    });
      $('#provinsi').html(provinsi);
  });

    $('#provinsi').on('change',function(event) {
      let id=$("#provinsi option:selected").attr('data-id');

      $.get('http://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi='+id, function(data) {
        kabupaten +='<option value="">Pilih kabupaten</option>';
        $.each(data, function(index, val) {
          $.each(val,function(index, el) {
            kabupaten +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
        });
      });
        $('#kabupaten').html(kabupaten);
    });
  });

    $('#kabupaten').on('change',function(event) {
      let id=$("#kabupaten option:selected").attr('data-id');

      $.get('https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota='+id, function(data) {
        kecamatan +='<option value="">Pilih kecamatan</option>';
        $.each(data, function(index, val) {
          $.each(val,function(index, el) {
            kecamatan +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
        });
      });
        $('#kecamatan').html(kecamatan);
    });
  });
    $('#kecamatan').on('change',function(event) {
      let id=$("#kecamatan option:selected").attr('data-id');

      $.get('https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan='+id, function(data) {
        kecamatan +='<option value="">Pilih kecamatan</option>';
        $.each(data, function(index, val) {
          $.each(val,function(index, el) {
            kelurahan +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
        });
      });
        $('#kelurahan').html(kelurahan);
    });
  });

});
</script>

<?php if ($this->session->flashdata('error')): ?>
  <script>
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      text: "<?=$this->session->flashdata('error')?>",
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
  })
</script>
<?php endif ?>

<?php if ($this->session->flashdata('warning')): ?>
  <script>
    Swal.fire({
      title: 'Galat',
      text: "File/foto yang anda upload tidak sesuai aturan",
      icon: 'warning',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OK'
  })
</script>
<?php endif ?>