<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <a href="<?=base_url()?>master/anggota/newdata" class="btn btn-primary btn-sm">Tambah Anggota</a>
                    <hr>
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead id="hd">
                                <tr>
                                    <th>#</th>
                                    <th>Foto</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($data as $i=>$d): ?>
                                    <?php $path = $this->db->get_where('tb_user', ['id_user'=>$d['user_id']])->row()->username?>
                                    <tr>
                                        <td>
                                            <?=$i+1?>
                                        </td>
                                        <td>
                                            <img style="border-radius:0px" src="<?=base_url()?>uploads/<?=$path?>/<?=$d['foto']?>"
                                            alt="">
                                        </td>
                                        <td>
                                            <p>Nama : <?=$d['nama_lengkap']?></p>
                                            <p>Asal wilayah dari kabupaten <?=$d['kabupaten']?> desa <?=$d['kecamatan']?>
                                        </p>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-sm view" data-toggle="modal"
                                        data-target="#view-<?=$d['id_anggota']?>">View</a>
                                        <a href="" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#hapus<?=$d['id_anggota']?>">Hapus</a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


<?php foreach($data as $m): ?>
    <?php 
    $path = $this->db->get_where('tb_user', ['id_user'=>$m['user_id']])->row()->username;
    ?>
    <div class="modal fade " id="view-<?=$m['id_anggota']?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Data Anggota</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row gutters-sm">
                        <div class="col-md-4 mb-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-column align-items-center text-center">
                                        <img src="<?=base_url()?>uploads/<?=$path?>/<?=$m['foto']?>" alt="Admin" class="rounded-circle" width="150">
                                        <div class="mt-3">
                                            <h5><?=$m['nama_lengkap']?></h5>
                                            <p class=" mb-1">
                                                <?=$m['jenis_anggota'] == 1 ? 'Anggota':'Pengurus'?>
                                            </p>
                                            <p class="text-muted font-size-md">
                                                <b><?=$m['nomor_anggota']?></b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <?php foreach ($form as $k => $v): ?>
                                        <?php if ($v->name != 'bukti_kepemilikan' && $v->name != 'foto'): ?>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h6 class="mb-0"><?=$v->label_name?></h6>
                                                </div>
                                                <div class="col-sm-8 text-muted">
                                                    <?=$m[$v->name]?>
                                                </div>
                                            </div>
                                            <hr> 
                                        <?php endif ?>      
                                    <?php endforeach ?>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hapus<?=$m['id_anggota']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body bg-danger text-white">
            <div class="row">
                <div class="col-sm-12">
                    <p>
                        Apa Anda Yakin Ingin Menghapus Data ini?
                    </p>
                </div>
                <div class="col-sm-12">
                    <a href="<?=base_url()?>master/anggota/delete/a/<?=$m['id_anggota']?>" class="btn btn-warning btn-sm float-right">YA</a>
                    <button class="btn btn-info btn-sm float-left" data-dismiss="modal">TIDAK</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php endforeach ?>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "lengthChange": true,
            "drawCallback": function(settings) {
            // $("#myTable thead").remove();
            // $("#myTable tfoot").remove();
            }
        });

        $('.view').on('click', function() {
            let id = $(this).data('id');
            $('#view').modal('show');
        })
    });
</script>

<?php if ($this->session->flashdata('success')): ?>
  <script>
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      text: "<?=$this->session->flashdata('success')?>",
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
  })

</script>
<?php endif ?>

<?php if ($this->session->flashdata('error')): ?>
  <script>
    Swal.fire({
      title: 'Galat',
      text: "<?=$this->session->flashdata('error')?>",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Login!'
  }).then((result) => {
      if (result.isConfirmed) {
        window.location.href="<?=base_url()?>user/login"
    }
})
</script>
<?php endif ?>

<?php if ($this->session->flashdata('warning')): ?>
  <script>
    Swal.fire({
      title: 'Galat',
      text: "File/foto yang anda upload tidak sesuai aturan",
      icon: 'warning',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OK'
  })
</script>
<?php endif ?>