<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Analytics | Notika - Notika Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
      ============================================ -->
      <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>assets/img/favicon.ico">
    <!-- Google Fonts
      ============================================ -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <!-- font awesome CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <!-- Notika icon CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/notika-custom-icon.css">
    <!-- mCustomScrollbar CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <!-- normalize CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/normalize.css">
	<!-- wave CSS
		============================================ -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/wave/waves.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/css/wave/button.css">
    <!-- Data Table JS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.dataTables.min.css">
    <!-- main CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
    <!-- style CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/style.css">
    <!-- responsive CSS
      ============================================ -->
      <link rel="stylesheet" href="<?=base_url()?>assets/css/responsive.css">
    <!-- modernizr JS
      ============================================ -->
      
      <style>
        body{
            background:#00c292;
        }
        .bg-success{
            background:#00c292;
        }
        .dataTables_wrapper .dataTables_filter{ width: 100% }
        .table > tbody > tr > td{
            vertical-align:middle;
        }

        /* TOGGLE STYLING */
.toggle {
  margin: 0 0 1.5rem;
  box-sizing: border-box;
  font-size: 0;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: stretch;
}
.toggle input {
  width: 0;
  height: 0;
  position: absolute;
  left: -9999px;
}
.toggle input + label {
  margin: 0;
  padding: 5px 6px;
  box-sizing: border-box;
  position: relative;
  display: inline-block;
  border: solid 1px #DDD;
  background-color: #FFF;
/*  font-size: 1rem;*/
  line-height: 140%;
  font-weight: 600;
  text-align: center;
  box-shadow: 0 0 0 rgba(255, 255, 255, 0);
  transition: border-color 0.15s ease-out, color 0.25s ease-out, background-color 0.15s ease-out, box-shadow 0.15s ease-out;
  /* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
  /*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
  /* ----- */
}
.toggle input + label:first-of-type {
  border-radius: 6px 0 0 6px;
  border-right: none;
}
.toggle input + label:last-of-type {
  border-radius: 0 6px 6px 0;
  border-left: none;
}
.toggle input:hover + label {
  border-color: #213140;
}
.toggle input:checked + label {
  background-color: #4B9DEA;
  color: #FFF;
  box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
  border-color: #4B9DEA;
  z-index: 1;
}
.toggle input:focus + label {
  outline: dotted 1px #CCC;
  outline-offset: 0.45rem;
}
@media (max-width: 800px) {
  .toggle input + label {
    padding: 0.75rem 0.25rem;
    flex: 0 0 50%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
}

/* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
.status {
  margin: 0;
  font-size: 1rem;
  font-weight: 400;
}
.status span {
  font-weight: 600;
  color: #B6985A;
}
.status span:first-of-type {
  display: inline;
}
.status span:last-of-type {
  display: none;
}
@media (max-width: 800px) {
  .status span:first-of-type {
    display: none;
  }
  .status span:last-of-type {
    display: inline;
  }
}
    </style>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
            <!-- Start Header Top Area -->
            <div class="header-top-area bg-success">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="logo-area">
                                <a href="#"><img src="<?=base_url()?>assets/img/apk-1.png" alt="" width="50"/></a>
                                <label for="">PORTAL KEANGGOTAAN APKASINDO</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Header Top Area -->
            <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li ><a  href="#Home"> Home</a>
                        </li>
                        <li><a  href="#mailbox"> Daftar Anggota</a>
                        </li>
                        <li><a  href="#Interface">Pembelian Kecambah</a>
                        </li>
                       
                    </ul>
                    <!-- <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="index.html">Dashboard One</a>
                                </li>
                                <li><a href="index-2.html">Dashboard Two</a>
                                </li>
                                <li><a href="index-3.html">Dashboard Three</a>
                                </li>
                                <li><a href="index-4.html">Dashboard Four</a>
                                </li>
                                <li><a href="analytics.html">Analytics</a>
                                </li>
                                <li><a href="widgets.html">Widgets</a>
                                </li>
                            </ul>
                        </div>
                        <div id="mailbox" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="inbox.html">Inbox</a>
                                </li>
                                <li><a href="view-email.html">View Email</a>
                                </li>
                                <li><a href="compose-email.html">Compose Email</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Interface" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="animations.html">Animations</a>
                                </li>
                                <li><a href="google-map.html">Google Map</a>
                                </li>
                                <li><a href="data-map.html">Data Maps</a>
                                </li>
                                <li><a href="code-editor.html">Code Editor</a>
                                </li>
                                <li><a href="image-cropper.html">Images Cropper</a>
                                </li>
                                <li><a href="wizard.html">Wizard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Charts" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="flot-charts.html">Flot Charts</a>
                                </li>
                                <li><a href="bar-charts.html">Bar Charts</a>
                                </li>
                                <li><a href="line-charts.html">Line Charts</a>
                                </li>
                                <li><a href="area-charts.html">Area Charts</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Tables" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="normal-table.html">Normal Table</a>
                                </li>
                                <li><a href="data-table.html">Data Table</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Forms" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="form-elements.html">Form Elements</a>
                                </li>
                                <li><a href="form-components.html">Form Components</a>
                                </li>
                                <li><a href="form-examples.html">Form Examples</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Appviews" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="notification.html">Notifications</a>
                                </li>
                                <li><a href="alert.html">Alerts</a>
                                </li>
                                <li><a href="modals.html">Modals</a>
                                </li>
                                <li><a href="buttons.html">Buttons</a>
                                </li>
                                <li><a href="tabs.html">Tabs</a>
                                </li>
                                <li><a href="accordion.html">Accordion</a>
                                </li>
                                <li><a href="dialog.html">Dialogs</a>
                                </li>
                                <li><a href="popovers.html">Popovers</a>
                                </li>
                                <li><a href="tooltips.html">Tooltips</a>
                                </li>
                                <li><a href="dropdown.html">Dropdowns</a>
                                </li>
                            </ul>
                        </div>
                        <div id="Page" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="contact.html">Contact</a>
                                </li>
                                <li><a href="invoice.html">Invoice</a>
                                </li>
                                <li><a href="typography.html">Typography</a>
                                </li>
                                <li><a href="color.html">Color</a>
                                </li>
                                <li><a href="login-register.html">Login Register</a>
                                </li>
                                <li><a href="404.html">404 Page</a>
                                </li>
                            </ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
            <!-- Breadcomb area Start-->

            <!-- Breadcomb area End-->
            <?php $this->load->view($page); ?>
            <!-- Start Footer area-->
            <!-- <div class="footer-copyright-area footer fixed-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="footer-copy-right">
                                <p>Copyright © 2018
                                    . All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- End Footer area-->
    <!-- jquery
      ============================================ -->
      <script src="<?=base_url()?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/counterup/jquery.counterup.min.js"></script>
      <script src="<?=base_url()?>assets/js/counterup/waypoints.min.js"></script>
      <script src="<?=base_url()?>assets/js/counterup/counterup-active.js"></script>
    <!-- sparkline JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/sparkline/jquery.sparkline.min.js"></script>
      <script src="<?=base_url()?>assets/js/sparkline/sparkline-active.js"></script>
    <!-- knob JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/knob/jquery.knob.js"></script>
      <script src="<?=base_url()?>assets/js/knob/jquery.appear.js"></script>
      <script src="<?=base_url()?>assets/js/knob/knob-active.js"></script>
    <!-- mCustomScrollbar JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- flot JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/flot/jquery.flot.js"></script>
      <script src="<?=base_url()?>assets/js/flot/jquery.flot.resize.js"></script>
      <script src="<?=base_url()?>assets/js/flot/jquery.flot.time.js"></script>
      <script src="<?=base_url()?>assets/js/flot/jquery.flot.tooltip.min.js"></script>
      <script src="<?=base_url()?>assets/js/flot/analtic-flot-active.js"></script>
	<!--  wave JS
		============================================ -->
        <script src="<?=base_url()?>assets/js/wave/waves.min.js"></script>
        <script src="<?=base_url()?>assets/js/wave/wave-active.js"></script>
    <!-- Data Table JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/data-table/jquery.dataTables.min.js"></script>
      <!-- <script src="<?=base_url()?>assets/js/data-table/data-table-act.js"></script> -->
    <!-- plugins JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/plugins.js"></script>
    <!-- main JS
      ============================================ -->
      <script src="<?=base_url()?>assets/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
        <!-- <script src="<?=base_url()?>assets/js/tawk-chat.js"></script> -->
        <script>
            $(document).ready( function () {
                $('#myTable').DataTable({
                    "lengthChange": false,
                    ajax: {
                        url: '<?=base_url()?>master/anggota/datatable',
                        dataSrc: ''
                    },
                    columns: [
                    {
                        data:"foto",
                        render:function(data){
                            return '<img width="75" src="<?=base_url()?>uploads/'+data+'" alt="">';
                        }
                    },
                    {
                        data : {
                            nama_lengkap:"nama_lengkap",
                            jenis_anggota:"jenis_anggota"
                        },
                        render:function(data) {
                            return ' <p>Nama :'+data.nama_lengkap+'</p><p>Jabatan : '+(data.jenis_anggota == 1? "Anggota":"Pengurus")+'</p>';
                        }
                    },
                    {
                        data:"nomor_anggota"
                    },
                    {
                        data:"provinsi"
                    }

                    ],
                    "drawCallback": function( settings ) {
                        $("#myTable thead").remove();
                        $("#myTable tfoot").remove();
                    } 
                });

                $('#keanggotaan').on('click', function(event) {
                    event.preventDefault();
                    $('#fm-keanggotaan').css('display', 'block');
                    $('#fm-penangkar').css('display', 'none');
                });
                $('#penangkar').on('click', function(event) {
                    event.preventDefault();
                    $('#fm-keanggotaan').css('display', 'none');
                    $('#fm-penangkar').css('display', 'block');
                });
            } );
        </script>
    </body>

    </html>
