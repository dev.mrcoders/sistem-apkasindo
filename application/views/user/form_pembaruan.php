<div class="content-wrapper">
    <div class="row">
        <div class="col-12 mb-4">
            <a href="<?=base_url()?>dashboard" class="btn btn-sm btn-warning"><i class="icon-arrow-left"></i> Back To dashboard</a>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Identitas Pendaftar</h4>
                </div>
                <form action="<?=base_url()?>user/dashboard/updateform" method="POST">
                    <input type="hidden" name="id_anggota" value="<?=$data['id_anggota']?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-example-int form-horizental mg-t-15">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <label class="hrzn-fm">Jenis Keanggotaan</label>
                                            </div>
                                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                                <div class="selectgroup w-100">
                                                    <label class="selectgroup-item">
                                                        <input type="radio" name="jenisanggota" <?=($data['jenis_anggota'] == 1? 'checked':'')?> value="1" class="selectgroup-input" checked="" required>
                                                        <span class="selectgroup-button">Anggota</span>
                                                    </label>
                                                    <label class="selectgroup-item">
                                                        <input type="radio" name="jenisanggota" <?=($data['jenis_anggota'] == 2? 'checked':'')?> value="2" class="selectgroup-input" required>
                                                        <span class="selectgroup-button">Pengurus</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($form as $i=> $f): ?>
                            <?php if ($i != 11 && $i != 17): ?>
                                <div class="form-group row m-0">
                                    <label class="col-sm-3 col-form-label"><?=$f->label_name?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="<?=$f->name?>" id="" required value="<?=$data[$f->name]?>">
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-sm btn-primary">Update Data</button>
                    </div>
                </form>
            </div>
            <div class="card mt-2">
                <div class="card-header">
                    <h4>File Uploads Pendaftar</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Jenis File</td>
                                    <td>File</td>
                                    <td>Act</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $Path = $this->db->get_where('tb_user', ['id_user'=>$data['user_id']])->row()->username; ?>
                                <tr>
                                    <td>Foto Profil</td>
                                    <td>
                                        <a data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['foto']?>" class="v-file"><?=$data['foto']?></a>
                                    </td>
                                    <td><button class="btn btn-sm btn-info gambar" data-jenis="foto_profil">Change</button></td>
                                </tr>
                                <tr>
                                    <td>Bukti Kepemilikan</td>
                                    <td>

                                        <a  data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['bukti_kepemilikan']?>" class="v-file"><?=$data['bukti_kepemilikan']?></a>
                                    </td>
                                    <td><button class="btn btn-sm btn-info file-pdf" data-jenis="bukti_kepemilikan">Change</button></td>
                                </tr>
                                <tr>
                                    <td>Bukti Pembayaran Pendaftaran</td>
                                    <td>
                                        <?php if ($data['bukti']==''): ?>
                                            <label for="" class="badge badge-danger">NoUpload</label>
                                        <?php else: ?>
                                            <a  data-file="<?=base_url()?>uploads/<?=$Path?>/<?=$data['bukti']?>" class="v-file"><?=$data['bukti']?></a>
                                        <?php endif ?>
                                    </td>
                                    <td><button class="btn btn-sm btn-info gambar" data-jenis="bukti_bayar">Change</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary btn-sm btn-block finish update">Update Data Selesai</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="view-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe frameborder="0" scrolling="no" width="100%" height="400px" src="" name="imgbox" id="img-view">
                    <p>iframes are not supported by your browser.</p>
                </iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="fm-gambar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Form Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?=base_url()?>user/dashboard/updatefile" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row py-4">
                        <div class="col-lg-12 ">
                            <input type="hidden" name="id_anggota" value="<?=$data['id_anggota']?>">
                            <input type="hidden" name="jenis_" id="jenis_">
                            <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                                <input id="upload" type="file" onchange="" name="file" class="form-control border-0">
                                <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file </label>
                                <div class="input-group-append">
                                    <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> <i class="fa fa-upload mr-2 text-muted"></i>
                                        <small class="text-uppercase font-weight-bold text-muted">Choose file</small>
                                    </label>
                                </div>
                            </div>
                            <small class="ml-2 text-danger"> *jpg,jpeg,png max size 5 Mb</small>
                            <small class="ml-2 " id="output"> </small>

                            <p class="font-italic text-white text-center">The image uploaded will be rendered inside the box below.</p>
                            <div class="image-area ">
                                <iframe frameborder="2" scrolling="no" width="100%" height="200px" src="" name="imgbox" id="imageResult">
                                    <p>iframes are not supported by your browser.</p>
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Upload File</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.v-file').click(function(event) {
        let file = $(this).data('file');
        $('#img-view').attr('src', file);
        $('#view-file').modal('show');
    });

    function readURL(input,ext) {
        var filePath = input.value;
        var allowedExtensions =ext;
        if (!allowedExtensions.exec(filePath)) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Type File salah, silahkan pilih ulang',
        }).then((result) => {
            if (result.isConfirmed) {
              input.value = '';
              return false;
          }
      })

    }else{
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imageResult')
          .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
  }
}
}

$(function () {

    var allowedExtensions;

    $('.gambar').on('click',function(argument) {
        allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        let jenis = $(this).data('jenis');
        $('#jenis_').val(jenis);
        $('#fm-gambar').modal('show');
    })

    $('.file-pdf').on('click',function(argument) {
        allowedExtensions = /(\.pdf)$/i;
        let jenis = $(this).data('jenis');
        $('#jenis_').val(jenis);
        $('#fm-gambar').modal('show');
    })

    $('.finish').on('click', function(event) {
        event.preventDefault();
        Swal.fire({
          title: 'Apa anda yakin mengakhiri perubahan data?',
          text: "Data akan di proses ulang olah admin",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, It Done!'
      }).then((result) => {
          if (result.isConfirmed) {
            window.location.href="<?=base_url()?>user/dashboard/finishupdated/<?=$data['id_anggota']?>";
        }
    })
  });

    
    $('#upload').on('change', function () {
       const size =(this.files[0].size / 1024 / 1024).toFixed(2);
       if (size > 5) {

        $("#output").html('<b>' +
          'This file size is: ' + size + " MB" + '</b>');

    }else{
        readURL(this,allowedExtensions);
    }

});
});
</script>

<?php if ($this->session->flashdata('success')): ?>
  <script>
    Swal.fire({
        position: 'top-end',
        title: 'sukses',
        text: "File berhasil Di Update",
        icon: 'success',
        showConfirmButton: false,
        timer: 1500
    })
</script>
<?php endif ?>
<?php if ($this->session->flashdata('warning')): ?>
  <script>
    Swal.fire({
        position: 'top-end',
        title: 'Galat',
        text: "File/foto yang anda upload tidak sesuai aturan",
        icon: 'warning',
        showConfirmButton: false,
        timer: 1500
    })
</script>
<?php endif ?>

<?php if ($this->session->flashdata('finish')): ?>
  <script>
    Swal.fire({
        icon: 'success',
        title: 'Berhasil',
        text: 'Semua data telah di perbarui',
        confirmButtonText: 'Kembali ke Dashboard!'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href="<?=base_url()?>dashboard";
    }
})
</script>
<?php endif ?>