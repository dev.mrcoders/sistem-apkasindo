
<div class="breadcomb-area ">
  <div class="container text-center">
    <h2 style="color:white;">FORMULIR PENDAFTARAN ANGGOTA <br>APKASINDO PROVINSI RIAU</h2>
</div>
</div>
<div class="dialog-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                <div class="dialog-inner" style="background: white;padding: 25px;border-radius: 20px;">
                    <div class="contact-hd dialog-hd">
                        <h2>Keanggotan APKASINDO</h2>
                        <p>Anda Akan Terdaftar Menjadi Anggota atau pengurus APKASINDO</p>
                    </div>
                    <div class="dialog-pro dialog">
                        <button class="btn btn-info" id="keanggotaan">Daftar Sekarang</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="dialog-inner sm-res-mg-t-30" style="background: white;padding: 25px;border-radius: 20px;">
                    <div class="contact-hd dialog-hd">
                        <h2>Penangkar</h2>
                        <p>Anda hanya Akan Terdaftar sebagai penangkar untuk pembelian Bibit</p>
                    </div>
                    <div class="dialog-pro dialog">
                        <button class="btn btn-info" id="penangkar">Daftar Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="fm-keanggotaan" style="display: none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form action="<?=base_url()?>user/registrasi/store/anggota" method="POST" enctype="multipart/form-data">
                    <div class="form-example-wrap mg-t-30">
                        <div class="cmp-tb-hd cmp-int-hd">
                            <h2>Formulir Pendaftaran Keanggotaan</h2>
                        </div>
                        <div class="form-example-int form-horizental mg-t-15">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                            <label class="hrzn-fm">Jenis Keanggotaan</label>
                                        </div>
                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                        <div class="toggle">
                                            <input type="radio" name="keanggotaan" value="1" id="sizeWeight" checked="checked" />
                                            <label for="sizeWeight">Anggota</label>
                                            <input type="radio" name="keanggotaan" value="2" id="sizeDimensions" />
                                            <label for="sizeDimensions">Pengurus</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php foreach($form_anggota as $fm): ?>
                            <div class="form-example-int form-horizental">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                            <label class="hrzn-fm"><?=$fm->label_name?></label>
                                        </div>
                                        <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
                                            <div class="nk-int-st">
                                                <input type="<?=$fm->type?>" class="form-control input-sm" name="<?=$fm->name?>" placeholder="Enter <?=$fm->label_name?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <div class="form-example-int form-horizental mg-t-15">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Username</label>
                                    </div>
                                    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control input-sm" name="username" placeholder="Username">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-example-int form-horizental mg-t-15">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Password</label>
                                    </div>
                                    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="password" class="form-control input-sm" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-example-int mg-t-15">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                </div>
                                <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
                                    <button class="btn btn-success notika-btn-success btn-lg" style="float:right;">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="fm-penangkar" style="display: none;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-wrap mg-t-30">
                    <div class="cmp-tb-hd cmp-int-hd">
                        <h2>Formulir Pendaftaran Penangkar</h2>
                    </div>
                    <form action="<?=base_url()?>user/registrasi/store/penangkar" method="POST" enctype="multipart/form-data">
                        <?php foreach($form_penangkar as $i=>$fp): ?>
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm"><?=$fp->label_name?></label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="<?=$fp->type?>" name="<?=$fp->name?>" class="form-control input-sm" placeholder="Enter <?=$fp->label_name?>" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Username</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control input-sm" placeholder="Enter username" name="username">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-example-int form-horizental mg-t-15">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Password</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="password" class="form-control input-sm" placeholder="Password" name="password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-example-int mg-t-15">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                    <button class="btn btn-success notika-btn-success btn-lg" style="float:right;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
