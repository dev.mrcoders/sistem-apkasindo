<div class="content-wrapper">

	<div class="row" >
		<div class="col-sm-12">
			<div class="card" >
				<div class="card-body" >
					<div class="container mb-5 mt-3">
						<div class="row d-flex align-items-baseline">
							<div class="col-xl-9">
								<p style="color: #7e8d9f;font-size: 20px;">
									Invoice >> 
									<strong id="inv">ID: #<?=$invoice->kode_invoice?></strong>
								</p>
							</div>

							<hr>
						</div>
						<div class="container" id="printableArea">
							<div class="col-md-12">
								<div class="text-center">
									<img src="<?=base_url()?>assets/img/logo.png" alt="" class="w-50">
								</div>
							</div>
							<div class="row">
								<table border="0" width="100%">
									<tr>
										<td width="65%">
											<div class="col-xl-12">
												<ul class="list-unstyled">
													<li class="text-muted">Kepada: <span style="color:#5d9fc5 ;"><?=$user->nama_lengkap?></span></li>
													<li class="text-muted"><?=$user->alamat_tinggal?>, <?=$user->kecamatan?></li>
													<li class="text-muted"><?=$user->kabupaten?>, <?=$user->provinsi?></li>
													<li class="text-muted"><i class="fas fa-phone"></i> <?=$user->telepon?></li>
												</ul>
											</div>
										</td>
										<td>
											<div class="col-xl-12">
												<p class="text-muted">Invoice</p>
												<ul class="list-unstyled">
													<li class="text-muted">
														<i class="fas fa-circle" style="color:#84B0CA ;"></i> 
														<span class="fw-bold">ID:</span>#<?=$invoice->kode_invoice?>
													</li>
													<li class="text-muted">
														<i class="fas fa-circle" style="color:#84B0CA ;"></i> 
														<span class="fw-bold">Creation Date: </span><?=date('d M Y',strtotime($invoice->created_at))?>
													</li>
													<li class="text-muted"><i class="fas fa-circle" style="color:#84B0CA ;"></i>
														<span class="me-1 fw-bold">Status:</span>
														<?php if ($invoice->status_pembayaran == 'unpaid'): ?>
															<span class="badge bg-warning text-black fw-bold">Unpaid</span>
														<?php elseif($invoice->status_pembayaran == 'proses'): ?>
															<span class="badge bg-info text-black fw-bold">Prosess</span>
														<?php else: ?>
															<span class="badge bg-success text-black fw-bold">Success</span>
														<?php endif ?>

													</li>
												</ul>
											</div>
										</td>
									</tr>
								</table>
								
								
							</div>
							<div class="row my-2 mx-1 justify-content-center">
								<table class="table table-striped table-borderless">
									<thead style="background-color:#84B0CA ;" class="text-white">
										<tr>
											<th scope="col">#</th>
											<th scope="col">Description</th>
											<th scope="col">Qty</th>
											<th scope="col">Unit Price</th>
											<th scope="col">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">1</th>
											<td>Beli Bibit Kecambah</td>
											<td><?=$transaksi->jumlah_bibit?></td>
											<td>Rp 2000</td>
											<td>Rp <?=$transaksi->total_harga?></td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3"></td>
											<td>SubTotal</td>
											<td>Rp <?=$transaksi->total_harga?></td>
										</tr>
										<tr>
											<td colspan="3"></td>
											<td>Total Amount</td>
											<td> <h4>Rp <?=$transaksi->total_harga?></h4></td>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="row">
								<div class="col-xl-8">
									<p>
										metode Pembayaran
										<ul>
											<li>Transfer :<br>
												Lakukan pembayaran Ke Rekening
												<ul>
													<li>BRI : 00293403023 an : asssss</li>
													<li>BTN : 00293403023 an : asssss</li>
												</ul>
											</li>
											<li>
												Pembayaran Dengan Cara Cash silahkan print invoice dan datang ke kantor apkasindo
											</li>
										</ul>


									</p>
								</div>
							</div>
							<hr>
							<div class="row">

								<div class="col-xl-2">

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function () {
    window.print();
	});

</script>