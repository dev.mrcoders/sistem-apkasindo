<div class="content-wrapper">
  <div class="row">
    <div class="col-sm-12 mb-4 mb-xl-0">
      <h4 class="font-weight-bold text-dark">PEMBELIAN BIBIT</h4> 
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card p-2">
        <?php if ($isAda > 0): ?>
          <div class="card-body">
            <a href="<?=base_url()?>user/transaksi/index/new" class="bnt btn-primary btn-sm">New Transaksi</a>
            <div class="table-responsive">
              <table class="table">
                
                <thead>
                  <tr>
                    <th>No Transaksi</th>
                    <th>No Invoice</th>
                    <th>Product</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($isTransasksi->result() as $key => $v): ?>
                    <tr>
                      <td><?=$v->kode_transaksi?></td>
                      <td>#<?=$v->kode_invoice?></td>
                      <td class="text-success">  <?=$v->jumlah_bibit?> Bibit</td>
                      <td>
                        <?php if ($v->status_pembayaran=='unpaid'): ?>
                          <a href="<?=base_url().'user/transaksi/invoice/'.$v->kode_transaksi?>" class="btn btn-default border btn-sm text-warning">
                            <?=$v->status_pembayaran?>
                          </a>
                        <?php elseif($v->status_pembayaran=='success'): ?>
                          <a  class="btn btn-default border btn-sm text-success">
                            <?=$v->status_pembayaran?>
                          </a>
                        <?php else: ?>
                          <a href="<?=base_url().'user/transaksi/invoice/'.$v->kode_transaksi?>" class="btn btn-default border btn-sm text-info">
                            <?=$v->status_pembayaran?>
                          </a>
                        <?php endif ?>
                        
                        
                      </td>
                    </tr>
                  <?php endforeach ?>
                  

                </tbody>
              </table>
            </div>
          </div>
        <?php else: ?>

          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <h4>Identitas Pembeli</h4>
                <hr>
                <div class="form-group row mb-0">
                  <label for="exampleInputUsername2" class="col-sm-5 col-form-label">Nama Lengkap</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->nama_lengkap?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputEmail2" class="col-sm-5 col-form-label">NIK KTP</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->nik_ktp?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputEmail2" class="col-sm-5 col-form-label">Alamat Tinggal</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->alamat_tinggal?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Kecamatan</label>
                  <div class="col-sm-7 ">
                    <input type="text" class="form-control" name="" value="<?=$data->kecamatan?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Kabupaten</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->kabupaten?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Provinsi</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->provinsi?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">No Handphone</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->telepon?>" readonly>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <form class="forms-sample" action="<?=base_url()?>user/transaksi/createtransaksi" method="POST" enctype="multipart/form-data">
                  <h4>File Syarat & transaksi Pembelian</h4>
                  <hr>
                  <?php foreach ($isFile as $key => $file): ?>
                    <div class="form-group row mb-0">
                      <label for="exampleInputMobile" class="col-sm-5 col-form-label"><?=$file->keterangan?> <i style="color:red;">*</i></label>
                      <div class="col-sm-7">
                        <input type="file" class="form-control file" name="<?=$file->nama_file?>" required>
                      </div>
                    </div>
                  <?php endforeach ?>
                  <div class="form-group row mb-0">
                    <label for="exampleInputMobile" class="col-sm-5 col-form-label">Jumlah Bibit Yang dibeli</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" name="jml_bibit" id="jml-bibit" min="400" minlength="4" required>
                    </div>
                  </div>
                  <div class="form-group row mb-0">
                    <label for="exampleInputMobile" class="col-sm-5 col-form-label">Total Harga</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control total" name="total_beli" id="total-harga" readonly="">
                    </div>
                  </div>
                  <div class="alert alert-info" role="alert">
                    <b>Informasi :</b>
                    <ul class="mb-0">
                      <li>File Harus bertipe pdf</li>
                      <li>Minimal pembelian 4 Ha 500 bibit</li>
                    </ul>
                  </div>
                  <button type="submit" class="btn btn-primary mr-2">Submit</button>
                  <a href="<?=base_url()?>user/transaksi" class="btn btn-light">Cancel</a>
                </form>

              </div>
            </div>
          </div>

        <?php endif ?>
        

      </div>
      
    </div>
  </div>
  
  
</div>

<script>
  function readURL(input,ext) {
    var filePath = input.value;
    var allowedExtensions =ext;
    if (!allowedExtensions.exec(filePath)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Type File salah, silahkan pilih ulang',
      }).then((result) => {
        if (result.isConfirmed) {
          input.value = '';
          return false;
        }
      })

    }else{
      return true;
    }

  }
  function onlyNumberKey(evt) {

    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
      return false;
    return true;
  }

  $(function() {
    $('#jml-bibit').attr('onkeypress', 'return onlyNumberKey(event)');

    let allowedExtensions = /(\.pdf)$/i;

    $('.file').on('change', function () {
     const size =(this.files[0].size / 1024 / 1024).toFixed(2);
     if (size > 5) {

      $("#output").html('<b>' +
        'This file size is: ' + size + " MB" + '</b>');

    }else{
      readURL(this,allowedExtensions);
    }

  });

    $('#jml-bibit').keyup(function(){
     var jml_bibit = parseFloat($(this).val()) || 0;
     let harga = 2000;

     $('#total-harga').val(numberToCurrency(jml_bibit * harga));
   });
  });

  var numberToCurrency = function (input_val, fixed = false, blur = false) {
    // don't validate empty input
    if(!input_val) {
      return "";
    }
    
    if(blur) {
      if (input_val === "" || input_val == 0) { return 0; }
    }

    if(input_val.length == 1) {
      return parseInt(input_val);
    }

    input_val = ''+input_val;
    
    let negative = '';
    if(input_val.substr(0, 1) == '-'){
      negative = '-';
    }
    // check for decimal
    if (input_val.indexOf(".") >= 0) {
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
      var decimal_pos = input_val.indexOf(".");

        // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
      left_side = left_side.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      if(fixed && right_side.length > 3) {
        right_side = parseFloat(0+right_side).toFixed(2);
        right_side =  right_side.substr(1, right_side.length);
      }

        // validate right side
      right_side = right_side.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        // Limit decimal to only 2 digits
      if(right_side.length > 2) {
        right_side = right_side.substring(0, 2);
      }

      if(blur && parseInt(right_side) == 0) {
        right_side = '';
      }

        // join number by .
        // input_val = left_side + "." + right_side;

      if(blur && right_side.length < 1) {
        input_val = left_side;
      } else {
        input_val = left_side + "." + right_side;
      }
    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
      input_val = input_val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    if(input_val.length > 1 && input_val.substr(0, 1) == '0' && input_val.substr(0, 2) != '0.' ) {
      input_val = input_val.substr(1, input_val.length);
    } else if(input_val.substr(0, 2) == '0,') {
      input_val = input_val.substr(2, input_val.length);
    }
    
    return negative+input_val;
  };
</script>

