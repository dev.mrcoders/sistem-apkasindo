<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Analytics | Notika - Notika Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>assets/img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/meanmenu/meanmenu.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/notika-custom-icon.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/normalize.css">
	<!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/wave/waves.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/wave/button.css">
    <!-- Data Table JS
		============================================ -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.dataTables.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
    
</head>

<body>
<div class="login-content">
        <!-- Login -->
        <div class="nk-block toggled" id="l-login">
            <div style="padding:10px;">
            <img src="<?=base_url()?>assets/img/apk-1.png" alt="" width="100"/><br>
            <h2 style="color:white;">APKASINDO </h2>
            </div>
            <form action="<?=base_url($path_checking)?>" method="POST">
            <div class="nk-form">
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" placeholder="Username" name="username">
                    </div>
                </div>
                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                    </div>
                </div>
                <div class="fm-checkbox">
                    <label><input type="checkbox" class="i-checks"> <i></i> Keep me signed in</label>
                </div>
                <button class="btn btn-success notika-btn-success btn-block">LOGIN</button>
            </div>
            </form>

       
        </div>
    </div>
	<!-- Breadcomb area End-->
    
    <!-- Start Footer area-->
  
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="<?=base_url()?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?=base_url()?>assets/js/counterup/waypoints.min.js"></script>
    <script src="<?=base_url()?>assets/js/counterup/counterup-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?=base_url()?>assets/js/sparkline/sparkline-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/knob/jquery.knob.js"></script>
    <script src="<?=base_url()?>assets/js/knob/jquery.appear.js"></script>
    <script src="<?=base_url()?>assets/js/knob/knob-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/flot/jquery.flot.js"></script>
    <script src="<?=base_url()?>assets/js/flot/jquery.flot.resize.js"></script>
    <script src="<?=base_url()?>assets/js/flot/jquery.flot.time.js"></script>
    <script src="<?=base_url()?>assets/js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?=base_url()?>assets/js/flot/analtic-flot-active.js"></script>
	<!--  wave JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/wave/waves.min.js"></script>
    <script src="<?=base_url()?>assets/js/wave/wave-active.js"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/data-table/jquery.dataTables.min.js"></script>
    <!-- <script src="<?=base_url()?>assets/js/data-table/data-table-act.js"></script> -->
    <!-- plugins JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?=base_url()?>assets/js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <!-- <script src="<?=base_url()?>assets/js/tawk-chat.js"></script> -->
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable({
                "lengthChange": false,
                "drawCallback": function( settings ) {
                    $("#myTable thead").remove();
                    $("#myTable tfoot").remove();
                } 
            });
        } );
    </script>
</body>

</html>
