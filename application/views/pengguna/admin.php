<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Data User Admin SISTEM </h4>
        </div>
        <div class="card-body">
          <button class="btn btn-sm btn-primary" data-toggle="modal" href='#new-user'>Add user</button>
          <hr>
          <div class="table-responsive">
            <table class="table display responsive nowrap" id="tb-report" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>level</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data->result() as $key => $row): ?>
                  <tr>
                    <td><?=$key+1?></td>
                    <td><?=$row->username?></td>
                    <td><?=$row->nama_level?></td>
                    <td>
                      <button class="btn btn-sm btn-info" data-toggle="modal" href='#edit-user<?=$row->id_user?>'>Edit</button>
                      <?php if ($row->level !=1): ?>
                        <button class="btn btn-sm btn-danger">Delete</button>
                      <?php endif ?>
                      
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php foreach ($data->result() as $key => $v): ?>
  <div class="modal fade" id="edit-user<?=$v->id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?=base_url()?>admin/user/updateuser">
        <div class="modal-body">
          <input type="hidden" name="id_user" value="<?=$v->id_user?>">
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-3 col-form-label">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" id="staticEmail" value="<?=$v->username?>">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-3 col-form-label">User Level</label>
            <div class="col-sm-9">
              <select name="level" id="" class="form-control">
                <option value="">==Pilih Level==</option>
                <?php foreach ($level->result() as $key => $lv): ?>
                  <option <?=($v->level == $lv->id_level ? 'selected':'') ?> value="<?=$lv->id_level?>"><?=$lv->nama_level?></option>
                <?php endforeach ?>
                
                
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach ?>

<div class="modal fade" id="new-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?=base_url()?>admin/user/createuser">
        <div class="modal-body">

          <div class="form-group row">
            <label for="staticEmail" class="col-sm-3 col-form-label">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" id="staticEmail" value="">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-3 col-form-label">User Level</label>
            <div class="col-sm-9">
              <select name="level" id="" class="form-control">
                <option value="">==Pilih Level==</option>
                <?php foreach ($level->result() as $key => $lv): ?>
                  <option value="<?=$lv->id_level?>"><?=$lv->nama_level?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
  $(function() {
    $('#tb-report').DataTable();
  });
</script>