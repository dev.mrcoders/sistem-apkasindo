<div class="content-wrapper">
  <div class="row">
    <div class="col-sm-12 mb-4 mb-xl-0">
      <h4 class="font-weight-bold text-dark">Data Transaksi Pembelian Kecambah</h4> 
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card p-2">

        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="data-tb">
              <thead>
                <tr>
                  <th>No Transaksi</th>
                  <th>No Invoice</th>
                  <th>Pembeli</th>
                  <th>Jenis Pembeli</th>
                  <th>Product</th>
                  <th>TGL Transaksi</th>
                  <th>Status</th>
                  <th>Act</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data as $key => $v): ?>
                  <?php $pembeli=$this->db->get_where('v_pembeli_transaksi',['user_id'=>$v->user_id])->row(); ?>
                  <tr>
                    <td><?=$v->kode_transaksi?></td>
                    <td><?=$v->kode_invoice?></td>
                    <td><?=$pembeli->nama_lengkap?></td>
                    <td><?=$pembeli->jenis?></td>
                    <td class="text-success"> <?=$v->jumlah_bibit?> bibit</td>
                    <td><?=date('d M Y',strtotime($v->created_at))?></td>
                    <td>
                      Status Bayar :
                      <?php if ($v->status_pembayaran == 'unpaid'): ?>
                        <label class="badge badge-warning">Unpaid</label>
                      <?php elseif($v->status_pembayaran == 'proses'): ?>
                        <label class="badge badge-info">Proses</label>
                      <?php else: ?>
                        <label class="badge badge-success">Success</label>
                      <?php endif ?>
                      <hr>
                      Status TRX :
                      <?php if ($v->status_beli == 'rejected'): ?>
                        <label class="badge badge-danger">rejected</label>
                      <?php elseif($v->status_beli == 'proses'): ?>
                        <label class="badge badge-info">Proses</label>
                      <?php else: ?>
                        <label class="badge badge-success">Success</label>
                      <?php endif ?>
                      

                    </td>
                    <td>
                      <button class="btn btn-info btn-sm p-1" data-toggle="modal" data-target="#v-data<?=$v->id_transaksi?>"><i class="fa fa-eye" ></i></button>
                      <a href="<?=base_url()?>admin/transaksi/invoice/<?=$v->kode_transaksi?>" class="btn btn-warning btn-sm p-1"><i class="fa fa-file-invoice" ></i></a>
                    </td>
                  </tr>
                <?php endforeach ?>
                

              </tbody>
            </table>
          </div>
        </div>

        <?php foreach ($data as $key => $vm): ?>
          <?php $cust=$this->db->get_where('v_pembeli_transaksi',['user_id'=>$vm->user_id])->row(); ?>
          <div class="modal fade" id="v-data<?=$vm->id_transaksi?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="staticBackdropLabel">Detail Pembeli</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="forms-sample">
                    <div class="row">
                      <div class="form-group col-sm-12">
                        <label for="exampleInputUsername1">Kode Transkasi</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->kode_transaksi?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputEmail1">NIK KTP</label>
                        <input type="email" class="form-control" readonly="" value="<?=$cust->nik_ktp?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputEmail1">Nama Pembeli</label>
                        <input type="email" class="form-control" readonly="" value="<?=$cust->nama_lengkap?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputPassword1">Jenis Pembeli</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->jenis?>">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Alamat</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->alamat_tinggal?>">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="exampleInputPassword1">Kecamatan</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->kecamatan?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputPassword1">Kabupaten</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->kabupaten?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputPassword1">Provinsi</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->provinsi?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputPassword1">No HP</label>
                        <input type="text" class="form-control" readonly="" value="<?=$cust->telepon?>">
                      </div>
                    </div>
                    <h4>Kelengkapan Dokumen</h4><hr>
                    <div class="row">
                      <table class="table">
                        <tr class="bg-primary text-white">
                          <th>No</th>
                          <th>Jenis File</th>
                          <th>File</th>
                        </tr>
                        <?php $file =$this->db->get_where('v_file_transaksi', ['kd_transaksi'=>$vm->kode_transaksi])->result();  ?>
                        <?php foreach ($file as $key => $fil): ?>
                          <?php 
                          $path = explode("/",$fil->file_path);
                          $path = base_url().'/'.$path[3].'/'.$path[4].'/'.$fil->file;
                          ?>
                          <tr>
                            <td><?=$key+1?></td>
                            <td><?=$fil->keterangan?></td>
                            <td><a class="btn-link file-view" data-file="<?=$path?>"><?=$fil->file?></a></td>
                          </tr>
                        <?php endforeach ?>
                        
                      </table>
                    </div><hr>
                    <h4>Invoce Transaksi</h4><hr>
                    <div class="row">
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Kode Invoice</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->kode_invoice?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Jumlah Bibit Di beli</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->jumlah_bibit?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Jumlah Yang Harus Dibayar</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->total_harga?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Rekening Pembayar</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->nama_rekening?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Jenis Pembayaran</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->jenis_pembayaran?>">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="exampleInputUsername1">Status Pembayaran</label>
                        <input type="text" class="form-control" readonly="" value="<?=$vm->status_pembayaran?>">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="exampleInputUsername1">Bukti Bayar</label><br>
                        <?php if($vm->file_path != ''): ?>
                          <?php 

                          $path_b = explode("/",$vm->file_path);
                          $path_b = base_url().'/'.$path_b[3].'/'.$path_b[4].'/'.$vm->bukti_pembayaran;
                          ?>
                          <a class="btn-link file-view" data-file="<?=$path_b?>"><?=$vm->bukti_pembayaran?></a>
                        <?php else:?>
                         <span class="badge badge-warning">Unpaid (belum Bayar)</span>
                       <?php endif?>
                     </div>
                     <div class="form-group col-sm-6">
                      <label for="exampleInputUsername1">Tanggal Pembayaran</label>
                      <input type="text" class="form-control" readonly="" value="<?=date('d M Y H:s',strtotime($vm->tgl_bayar))?>">
                    </div>
                  </div>
                  <hr>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary verifikasi" data-idtrx="<?=$vm->kode_invoice?>">Verifikasi Transaksi</button>
              </div>
            </div>
          </div>
        </div>

          <!-- <div class="modal fade" id="v-verifikasi<?=$vm->kode_invoice?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Form Verifikasi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="form-horizontal">
                    <hr />
                    <div class="form-group row">
                      <label for="ID" class="control-label col-sm-3">ID INVOICE:</label>
                      <div class="form-control-static col-sm-2">
                       <label>1123</label>
                     </div>
                     <label for="ID" class="control-label col-sm-4">KODE TRANSAKSI:</label>
                     <div class="form-control-static col-sm-3">
                       <label>1123</label>
                     </div>
                   </div>
                   <div>
                     <table class="table">
                      <thead>
                        <tr>
                         <th>Product</th>
                         <th>Qty</th>
                         <th>Price</th>
                         <th>total Price</th>
                       </tr>
                     </thead>

                     <tbody>
                       <tr>
                         <td>Bibit Sawit</td>
                         <td>1000</td>
                         <td>2000</td>
                         <td>20.000.000</td>
                       </tr>
                     </tbody>
                     <tfoot>
                       <tr class="font-weight-bold">
                         <td colspan="3">Ammount</td>
                         <td>Rp 20.000.000</td>
                       </tr>
                     </tfoot>
                   </table>
                 </div>
                 <div>
                   
                 </div>



               </form>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div> -->

    <?php endforeach ?>

  </div>

</div>
</div>
</div>

<div class="modal fade" id="v-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe frameborder="2"  scrolling="no" width="100%" height="800px" src="" name="imgbox" id="imageResult">
          <p>iframes are not supported by your browser.</p>
        </iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(function() {

    $('#data-tb').DataTable({
      responsive:true
    });

    $('.file-view').on('click',function(event) {
      event.preventDefault();
      let file=$(this).data('file');
      $('#imageResult').attr('src', file);
      $('#v-file').modal('show');
    });

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success m-2',
        cancelButton: 'btn btn-danger m-2'
      },
      buttonsStyling: false
    })

    $('.verifikasi').on('click', function(event) {
      event.preventDefault();
      let id_trx = $(this).data('idtrx');
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "Akan Memverifikasi Transaksi ini",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Accepted Transaksi',
        cancelButtonText: 'No, Rejected Transaksi!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          window.location.href="<?=base_url()?>admin/transaksi/verifikasi/"+id_trx+"/accepted";
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          window.location.href="<?=base_url()?>admin/transaksi/verifikasi/"+id_trx+"/rejected";
        }
      })
    });
  });

  function RejectedTransaksi(argument) {

  }
</script>