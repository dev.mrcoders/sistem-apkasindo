<?php $url = base_url().'dist'?>
<!doctype html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Content-Language" content="en" />
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#4188c9">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
  <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
  <!-- Generated: 2018-04-06 16:27:42 +0200 -->
  <title>APKASINDO</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css"> -->
  <!-- Dashboard Core -->
  <link href="<?=$url?>/assets/css/dashboard.css" rel="stylesheet" />

  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <!-- <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script> -->
  
  <style>
    .green-site{
      background:#288A57;
    }
    .nav-tabs .nav-link.active {
      border-color: #288A57;
      color: #288A57;
      background: transparent;
    }
    .img-bg{
      background-repeat: no-repeat;
      background-image: url('<?=base_url()?>assets/img/bg-sawit.jpg');
      background-size: cover;
    }
    .logo-img{
      background-color: white;
      border-radius: 100%;
    }
    .header-brand{
      font-size: 2rem;
    }
  </style>
</head>
<body class="">
  <div class="page">
    <div class="page-main green-site img-bg" >
      <div class="header py-4 green-site">
        <div class="container">
          <div class="d-flex text-white">
            <a class="header-brand" href="./index.html">
              <img src="<?=base_url()?>assets/img/apk-1.png" class="header-brand-img logo-img " alt="tabler logo">
              APKASINDO
            </a>
            <div class="d-flex order-lg-2 ml-auto">
              <div class="nav-item d-none d-md-flex">
                <a href="<?=base_url()?>user/login" class="btn btn-sm btn-primary" >LOGIN</a>
              </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
              <span class="header-toggler-icon"></span>
            </a>
          </div>
        </div>

      </div>
      <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-lg order-lg-first">
              <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                <!-- <li class="nav-item">
                  <a href="<?=base_url()?>" class="nav-link <?= ($this->uri->segment(1) == ''? 'active':'')?>"> Home </a>
                </li> -->
                <li class="nav-item">
                  <a href="<?=base_url()?>" class="nav-link <?= ($this->uri->segment(1) == ''? 'active':'')?>"> Pendaftaran Anggota</a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url()?>beli-bibit" class="nav-link <?= ($this->uri->segment(1) == 'beli-bibit'? 'active':'')?>"> Pembelian Bibit</a>
                </li>

              </ul>
              <a href="<?=base_url()?>user/login" class="btn btn-sm btn-primary d-lg-none d-sm-block mb-3" >LOGIN</a>
            </div>
          </div>
        </div>
      </div>

      <!-- div for page content -->
      <div class="my-3 my-md-5">
        <div class="container" style="min-height: 100vh;">
          <?php $this->load->view($page); ?>
        </div>
      </div>
      <!-- end div page content -->
    </div>

    <footer class="footer green-site">
      <div class="container">
        <div class="row align-items-center flex-row-reverse">
          <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center" style="color:white">
            Copyright © 2018 . Theme by Bootrap All rights reserved.
          </div>
        </div>
      </div>
    </footer>
  </div>
  <script src="<?=$url?>/assets/js/require.min.js"></script>
  <script>
    requirejs.config({
      baseUrl: '<?=$url?>'
    });
  </script>
  <script src="<?=$url?>/assets/js/dashboard.js"></script>
</body>
</html>