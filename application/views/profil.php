<?php 
$this->db->select('a.username,b.jenis_anggota,b.nomor_anggota,b.nik_ktp,b.nama_lengkap,b.telepon,b.foto,b.alamat_tinggal');
$this->db->join('tb_user a', 'a.id_user=b.user_id', 'inner');
$DetailUser = $this->db->get_where('tb_anggota b',['b.user_id'=>$this->session->userdata('user_id')])->row(); 
?>
<div class="row gutters-sm">
  <div class="col-md-4 mb-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-column align-items-center text-center">
          <img src="<?=base_url()?>uploads/<?=$DetailUser->username?>/<?=$DetailUser->foto?>" alt="Admin" class="rounded-circle" width="100">
          <div class="mt-3">
            <h4><?=$this->session->userdata('username');?></h4>
            <p class="text-secondary mb-1"><?=$DetailUser->jenis_anggota == 1? 'ANGGOTA':'PENGURUS'?></p>
            <p class="text-muted font-size-sm"><?=$DetailUser->nomor_anggota?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="card mt-3">
      
    </div>
  </div>
  <div class="col-md-8">
    <div class="card mb-3">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">No Anggota</h6>
          </div>
          <div class="col-sm-9 text-muted">
            <?=$DetailUser->nomor_anggota?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">NIK KTP</h6>
          </div>
          <div class="col-sm-9 text-muted">
            <?=$DetailUser->nik_ktp?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Nama Lengkap</h6>
          </div>
          <div class="col-sm-9 text-muted">
            <?=$DetailUser->nama_lengkap?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">No HP</h6>
          </div>
          <div class="col-sm-9 text-muted">
            <?=$DetailUser->telepon?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Alamat</h6>
          </div>
          <div class="col-sm-9 text-muted">
            <?=$DetailUser->alamat_tinggal?>
          </div>
        </div>
        <hr>
        
      </div>
    </div>
  </div>
</div>