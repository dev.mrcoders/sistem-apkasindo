<?php 
if ($this->session->userdata('level')==4) {
  $GetData = $this->db->get_where('v_new_anggota',['user_id'=>$this->session->userdata('user_id')])->row();
}elseif($this->session->userdata('level')==3 || $this->session->userdata('level')==5){
  $GetData = $this->db->get_where('tb_pembeli',['user_id'=>$this->session->userdata('user_id')])->row();
}


?>
<nav class="sidebar sidebar-offcanvas" id="sidebar" style="background:#00c292">
  <div class="user-profile">
    <div class="user-image">
      <?php if ($this->session->userdata('level')== 1 || $this->session->userdata('level')== 2 || $this->session->userdata('level')== 6): ?>
        <img src="<?=base_url()?>assets/img/admin.png">
      <?php else: ?>
      <img src="<?=base_url()?>uploads/<?= $this->session->userdata('username')?>/<?=$GetData->foto?>">
    <?php endif ?>
    </div>
    <div class="user-name">
      <?=$this->session->userdata('username')?>
    </div>
  </div>
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>dashboard">
        <i class="icon-box menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <?php
      if ($this->session->userdata('level')==1 || $this->session->userdata('level')==1 ) {
        $show = '';
      }elseif($this->session->userdata('level')==4){
        $verifikasi = $this->db->get_where('v_new_anggota',['user_id'=>$this->session->userdata('user_id')])->row();
        if ($verifikasi->status == 0 || $verifikasi->status == 2) {
          $show ='hidden';
        }else{
          $show = '';
        }
      }else{
        $show = '';
      }
     ?>
    <?php 
    $this->db->order_by('urut', 'asc');
    $mainmenu = $this->db->get_where('v_main_menu',['id_level'=>$this->session->userdata('level')])->result()?>
    <?php foreach ($mainmenu as $i => $mn):?>
      <?php $submenu = $this->db->get_where('v_sub_main',['id_level'=>$this->session->userdata('level'),'menu_id'=>$mn->menu_id])?>
      <?php if($submenu->num_rows() > 0): ?>
        <li class="nav-item" <?=$show?>>
          <a class="nav-link" data-toggle="collapse" href="#ui-basic<?=$mn->menu_id?>" aria-expanded="false" aria-controls="ui-basic">
            <i class="icon-disc menu-icon"></i>
            <span class="menu-title"><?=$mn->nama_menu?></span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="ui-basic<?=$mn->menu_id?>">
            <ul class="nav flex-column sub-menu">

              <?php foreach($submenu->result() as $sm): ?>
                <li class="nav-item"> <a class="nav-link" href="<?=base_url($sm->url)?>"><?=$sm->nama_menu?></a></li>
              <?php endforeach ?>
            </ul>
          </div>
        </li>
      <?php else: ?>
        <li class="nav-item" <?=$show?>>
          <a class="nav-link" href="<?=base_url($mn->url)?>">
            <i class="icon-box menu-icon"></i>
            <span class="menu-title"><?=$mn->nama_menu?></span>
          </a>
        </li>
      <?php endif ?>
    <?php endforeach ?>

  </ul>
</nav>