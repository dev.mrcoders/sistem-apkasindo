<!-- partial:partials/_navbar.html -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center" style="background:#00c292">
   <a class="navbar-brand brand-logo" href="index.html">
      <label class="font-weight-bold text-white"> APKASINDO </label>
  </a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end" style="background:#00c292">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="icon-menu"></span>
    </button>

    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item dropdown d-flex mr-4 ">
        <a href="<?=base_url()?>admin/login/logout" class="btn btn-danger btn-sm">
          LOGOUT
        </a>

      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="icon-menu"></span>
    </button>
  </div>
</nav>