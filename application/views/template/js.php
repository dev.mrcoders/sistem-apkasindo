<!-- base:js -->
<script src="<?=$url?>vendors/base/vendor.bundle.base.js"></script>
<script src="<?=$url?>js/simple.money.format.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?=$url?>js/off-canvas.js"></script>
  <script src="<?=$url?>js/hoverable-collapse.js"></script>
  <script src="<?=$url?>js/template.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <script src="<?=$url?>vendors/chart.js/Chart.min.js"></script>
  <script src="<?=$url?>vendors/jquery-bar-rating/jquery.barrating.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <script src="<?=$url?>js/dashboard.js"></script>
  <!-- End custom js for this page-->

  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowgroup/1.3.0/js/dataTables.rowGroup.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>

</body>

</html>