<!DOCTYPE html>
<html lang="en">
<?php $url = base_url().'admin/'?>
<head>
  <!-- Required meta tags --> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?=($title? $title:"ADMIN SISTEM")?></title>
  <?php include_once "css.php" ?>
</head>
<body>
  <?php $metod = $this->router->fetch_method(); ?>
  <div class="container-scroller">
    <?php if ($metod != 'download'): ?>
      <?php include_once "header.php" ?>
    <?php endif ?>

    <?php 
      $style = $metod != 'download'? '':'style="padding-top:0"';
    ?>
    
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" <?=$style?>>
      <!-- partial:partials/_sidebar.html -->
      <?php if ($metod != 'download'): ?>
        <?php include_once "navbar.php" ?>
      <?php endif ?>

      
      <?php if ($metod != 'download'): ?>
        <!-- partial -->
        <div class="main-panel">
          <?php $this->load->view($page); ?>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->

          <?php include_once "footer.php" ?>




          <!-- partial -->
        </div>
      <?php else: ?>
        <div class="main-panel" style="width: 100%;">
          <?php $this->load->view($page); ?>
          <!-- partial -->
        </div>
      <?php endif ?>
      
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <?php include_once "js.php" ?>

</body>

</html>

