<style>
  .dataTables_wrapper .dataTables_filter{ width: 100% }
  .table > tbody > tr > td{
    vertical-align:middle;
  }
</style>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="text-wrap p-lg-6">
          <h2 class="mt-0 mb-4">pendaftaran Anggota APKASINDO se-Indonesia</h2>

          <p class="text-justify">Dalam rangka mempercepat terwujudnya perkebunan kelapa sawit rakyat yang berkelanjutan, dibutuhkan sebuah satu sistem pendataan/database dan keseragaman Kartu Tanda Anggota (KTA) yang memuat data petani kelapa sawit anggota APKASINDO seluruh Indonesia, yang berguna untuk memudahkan penyampaian dan penyaluran informasi, kegiatan-kegiatan, regulasi, kendala lapangan, serta program-program strategis baik dari pemerintah maupun sektor pengusaha yang berkaitan dengan perkebunan kelapa sawit rakyat. </p>

          <h2 class="mt-0 mb-4">PERSYARATAN MENJADI ANGGOTA APKASINDO</h2>
          <ol>
            <li>Memiliki kartu tanda penduduk (eKTP)</li>
            <li>Memiliki kebun kelapa sawit</li>
            <li>Membayar biaya administrasi sebesar Rp25.000</li>
            <li>Mengisi formulir pendaftaran</li>
          </ol>

          <a href="<?=base_url()?>formulir-pendaftaran"> <h2><span>DAFTAR MENJADI ANGGOTA? </span> KLIK DISINI </h2></a>
          

        </div>
        <div class="p-lg-6">
          <hr>
        <h2 class="mt-0 mb-4">Anggota APKASINDO</h2>
        
        <div class="table-responsive">
          <table class="table card-table table-vcenter text-nowrap" id="table-id">
            <thead>
              <tr>
                <th class="w-1">No.</th>
                <th>Invoice Subject</th>
                <th>Client</th>
                <th>VAT No.</th>
                
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="avatar d-block" style="background-image: url(<?=base_url()?>dist/demo/faces/female/26.jpg)">
                  </td>
                  <td>
                    <div>Elizabeth Martin</div>
                    <div class="small text-muted">
                      Anggota
                    </div>
                  </td>
                  <td>
                    2003943003
                  </td>
                  <td>
                    Pekanbaru
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

  <script>

    $(document).ready(function() {
      $('#table-id').DataTable({
        "lengthChange": false,
        ajax: {
          url: '<?=base_url()?>master/anggota/datatable',
          dataSrc: ''
        },
        columns: [
        {
          data:{
            foto:"foto",
            username:"username"
          },
          render:function(data){
            return '<div class="avatar d-block" style="background-image: url(<?=base_url()?>uploads/'+data.username+'/'+data.foto+')">';
          }
        },
        {
          data : {
            nama_lengkap:"nama_lengkap",
            jenis_anggota:"jenis_anggota"
          },
          render:function(data) {
            return '<div>'+data.nama_lengkap+'</div>'
            +'<div class="small text-muted">'
            +(data.jenis_anggota == 2? "Anggota":"Pengurus")+
            '</div> ';
          }
        },
        {
          data:"nomor_anggota"
        },
        {
          data:"provinsi"
        }

        ],
        "drawCallback": function( settings ) {
          $("#table-id thead").remove();
          $("#table-id tfoot").remove();
        } 
      });
    } );

  </script>

