<div class="row">
  <div class="col-lg-12">
    <div class="card">
    <div class="text-wrap p-lg-6">
      <!-- UNTUK LEBAR CARD -->
          <div class="card mb-6" style="max-width: 1440px;"> 
          
        <div class="row g-0">
          <!-- UNTUK UKURAN FOTO -->
          <div class="col-md-4 ">
            <img src="<?=base_url()?>assets/img/Brosur.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <!-- UNTUK BAGIAN TEKS -->
          <div class="col-md-8">
            <div class="card-body">
            <div class="text-wrap p-lg-0">
              <h2 class="mt-0 mb-2">INFORMASI KECAMBAH </h2>
              <p class="card-text" style="text-align:justify">Benih-benih pilihan yang terseleksi dari PT. Dami Mas memberikan hasil yang luar biasa untuk Anda. Selama lebih dari 20 tahun, 
              benih Dami Mas memberikan kemudahan bagi pekebun di seluruh Indonesia dengan beragam keunggulannya seperti masa panen yang lebih singkat, produktivitas lebih tinggi, dan daya tahan terhadap penyakit yang lebih baik.
              Benih Dami Mas dipercaya oleh pimpinan industri kelapa sawit, karena mampu menghasilkan buah kelapa sawit yang bermutu tinggi hanya dalam waktu tiga tahun, dibanding rata-rata panen industri empat tahun. 
              Benih Dami Mas menghasilkan panen lebih besar dan tingkat ekstraksi minyak sawit di atas 25%.</p>
              <h2 class="mt-0 mb-4">Keunggulan Kecambah D X P DAMI MAS</h2>
          <ol>
            <li>Produksi Setelah 24 Bulan = 8-10 Ton/Ha/Tahun </li>
            <li>Produksi Setelah 3-8 Tahun = 36 Ton/Ha/Tahun</li>
            <li>Tahan Di Segala Jenis Lahan dan Tahan Penyakit</li>
            <li>Tingkat Kontaminasi Dura Dilapangan 0.1%</li>
            <li>Tegakan Sebanyak 136 Pohon/Hektar</li>
          </ol>
              <a href="<?=base_url()?>formulir-beli-bibit" class="btn btn-primary">BELI BIBIT DISINI</a>
            </div>
            </div>
          </div>
        </div>
      </div>    
    </div>

        <div class="p-lg-6">
        
        
        
        </div>
      </div>
    </div>
  </div>