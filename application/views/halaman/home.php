<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="text-wrap p-lg-6">
          <h2 class="mt-0 mb-4">Deskripsi</h2>
          <p class="text-justify">APKASINDO adalah organisasi profesi petani kelapa sawit yang disahkan dan dibina oleh Kementerian Pertanian RI cq Direktorat Jenderal Perkebunan dan Badan Eksekutif Gabungan Asosiasi Petani Perkebunan Republik Indonesia (GAPPERINDO) dan Dewan Minyak Sawit Indonesia (DMSI). APKASINDO terbentuk pada tahun 2000, difasilitasi Pemerintah c/q Kementerian Pertanian Republik Indonesia, sebagai Wadah Pemersatu Petani Kelapa Sawit Indonesia. APKASINDO saat ini telah tersebar di 22 provinsi dan 144 Kabupaten/Kota penghasil kelapa sawit di Indonesia mulai dari Nangroe Aceh Darussalam sampai Provinsi Papua.</p>
          <h2>Visi APKASINDO</h2>
          <p class="text-justify">“Menghantar Indonesia sebagai penghasil kelapa sawit terkemuka di dunia “ melalui pemberdayaan sumber daya, teknologi dan industri secara berkesinambungan yang berwawasan lingkungan.</p>
          <h2>Misi APKASINDO</h2>
          <ul>
            <li>
              Meningkatkan kualitas sumber daya petani dalam upaya penerapan paket teknologi baru kelapa sawit sehingga lebih efisien dan berdaya guna. 
            </li>
            <li>
              Mendorong para pelaku usaha produksi kelapa sawit sehingga terciptanya suatu sinergi yang terintegrasi dalam sistem agribisnis. 
            </li>
            <li>
              Membantu mewujudkan hak-hak para petani disekitar perusahaan perkebunan untuk mendapatkan pembangunan kebun sawit masyarakat minimal 20% dari total luas areal yang diusahakan perusahaan perkebunan untuk meningkatkan kesejahteraan masyarakat petani sawit secara nyata. 
            </li>
            <li>
              Memberikan perlindungan Advokasi hukum kepada petani kelapa sawit di seluruh wilayah Republik Indonesia. 
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>