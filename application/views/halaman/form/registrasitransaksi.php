<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <form action="<?=base_url()?>registrasi" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-sm-12">

              <div class="form-example-int form-horizental mg-t-15">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <label class="hrzn-fm">Jenis Pembeli</label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                      <div class="selectgroup w-100">
                        <label class="selectgroup-item">
                          <input type="radio" name="jenispembeli" value="1" class="selectgroup-input" checked="" required>
                          <span class="selectgroup-button">Petani</span>
                        </label>
                        <label class="selectgroup-item">
                          <input type="radio" name="jenispembeli" value="2" class="selectgroup-input" required>
                          <span class="selectgroup-button">Penangkar</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php foreach($form_anggota as $fm): ?>
            <div class="row mb-2">
              <label for="" class="col-sm-3"><?=$fm->label_name?></label>
              <div class="col-sm-9">
                <?php if ($fm->name == 'provinsi' || $fm->name == 'kabupaten' || $fm->name == 'kecamatan'): ?>
                  <select name="<?=$fm->name?>" id="<?=$fm->name?>" class="form-control" required>
                    <option>Pilih</option>
                  </select>
                <?php else: ?>
                  <input type="<?=$fm->type?>" class="form-control input-sm" name="<?=$fm->name?>" placeholder="Enter <?=$fm->label_name?>" required id="<?=$fm->name?>">
                <?php endif ?>
              </div>
            </div>
          <?php endforeach ?>
          <hr>
          <div class="row mt-2">
            <label for="" class="col-sm-3">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" required>
            </div>
          </div>
          <div class="row mt-2">
            <label for="" class="col-sm-3">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" required>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <a href="<?=base_url()?>daftar-anggota" class="btn btn-default btn-lg" style="float:left;">Cancel</a>
            </div>
            <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
              <button class="btn btn-success btn-lg" style="float:right;">Registrasi</button>
              
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(function() {
    let provinsi='';
    let kabupaten='';
    let kecamatan='';
    $('#nik_ktp').attr('onkeypress', 'return onlyNumberKey(event)');
    $.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi', function(data) {
      provinsi +='<option value="">Pilih Provinsi</option>';
      $.each(data, function(index, val) {
        $.each(val,function(index, el) {
          provinsi +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
        });
      });
      $('#provinsi').html(provinsi);
    });

    $('#provinsi').on('change',function(event) {
      let id=$("#provinsi option:selected").attr('data-id');

      $.get('http://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi='+id, function(data) {
        kabupaten +='<option value="">Pilih kabupaten</option>';
        $.each(data, function(index, val) {
          $.each(val,function(index, el) {
            kabupaten +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
          });
        });
        $('#kabupaten').html(kabupaten);
      });
    });

    $('#kabupaten').on('change',function(event) {
      let id=$("#kabupaten option:selected").attr('data-id');
      console.log(id);
      $.get('https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota='+id, function(data) {
        kecamatan +='<option value="">Pilih kecamatan</option>';
        $.each(data, function(index, val) {
          $.each(val,function(index, el) {
            kecamatan +='<option data-id="'+el.id+'" value="'+el.nama+'">'+el.nama+'</option>';
          });
        });
        $('#kecamatan').html(kecamatan);
      });
    });

  });

  function onlyNumberKey(evt) {

    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
      return false;
    return true;
  }
</script>

<?php if ($this->session->flashdata('success')): ?>
  <script>
    Swal.fire({
      title: 'sukses',
      text: "Anda Berhasil Registrasi, Silahkan Login untuk melakukan transaksi pembelian bibit",
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'LOGIN'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href="<?=base_url()?>user/login"
      }
    })
  </script>
<?php endif ?>

<?php if ($this->session->flashdata('error')): ?>
  <script>
    Swal.fire({
      title: 'Galat',
      text: "<?=$this->session->flashdata('error')?>, silahkan login untuk melakukan transaksi pembelian bibit",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Login!'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href="<?=base_url()?>user/login"
      }
    })
  </script>
<?php endif ?>

<?php if ($this->session->flashdata('warning')): ?>
  <script>
    Swal.fire({
      title: 'Galat',
      text: "File/foto yang anda upload tidak sesuai aturan",
      icon: 'warning',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OK'
    })
  </script>
  <?php endif ?>