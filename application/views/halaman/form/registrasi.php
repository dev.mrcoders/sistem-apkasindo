<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <form action="<?=base_url()?>registrasi" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-example-int form-horizental mg-t-15">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <label class="hrzn-fm">Jenis Keanggotaan <small class="text-danger">*</small></label>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                      <div class="selectgroup w-100">
                        <label class="selectgroup-item">
                          <input type="radio" name="jenisanggota" value="1" class="selectgroup-input" required>
                          <span class="selectgroup-button">Pengurus</span>
                        </label>
                        <label class="selectgroup-item">
                          <input type="radio" name="jenisanggota" value="2" class="selectgroup-input" checked=""
                            required>
                          <span class="selectgroup-button">Anggota</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mb-2">
                <label for="" class="col-sm-3">Keanggotaan</label>
                <div class="col-sm-9">
                  <select name="id_jenis_pengurus" id="id_jenis_pengurus" class="form-control">
                    <option value="">Pilih Kepengurusan</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <?php foreach($form_anggota as $fm): ?>
          <div class="row mb-2">
            <label for="" class="col-sm-3"><?=$fm->label_name?> <small class="text-danger">*</small></label>
            <div class="col-sm-9">
              <?php if ($fm->name == 'provinsi' || $fm->name == 'kabupaten' || $fm->name == 'kecamatan' || $fm->name == 'kelurahan'): ?>
              <select name="<?=$fm->name?>" id="<?=$fm->name?>" class="form-control" required>
                <option>Pilih <?=$fm->label_name?></option>
              </select>
              <?php else: ?>
              <input type="<?=$fm->type?>" class="form-control input-sm" id="<?=$fm->name?>" name="<?=$fm->name?>"
                placeholder="Enter <?=$fm->label_name?>" required>
              <?php endif ?>
              <?php if ($fm->name == 'bukti_kepemilikan'): ?>
              <small class="text-danger ml-2">Type File pdf, Max Size 5 Mb</small>
              <?php elseif($fm->name == 'foto'): ?>
              <small class="text-danger ml-2">Type File jpeg,jpg,png, Max Size 5 Mb</small>
              <?php endif ?>

            </div>
          </div>
          <?php endforeach ?>
          <hr>
          <div class="row mt-2">
            <label for="" class="col-sm-3">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" required>
            </div>
          </div>
          <div class="row mt-2">
            <label for="" class="col-sm-3">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" required>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <a href="<?=base_url()?>daftar-anggota" class="btn btn-default btn-lg" style="float:left;">Cancel</a>
            </div>
            <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12">
              <button class="btn btn-success btn-lg" style="float:right;">Registrasi</button>

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    let provinsi = '';
    let kabupaten = '';
    let kecamatan = '';
    let kelurahan = '';
    let kepengurusan = '';

    $('#nik_ktp').attr('onkeypress', 'return onlyNumberKey(event)');

    $.get("<?=base_url()?>welcome/jeniskepengurusan",
      function (data, textStatus, jqXHR) {
        $.each(data, function (indexInArray, valueOfElement) {
          // console.log(valueOfElement.jenis);
          kepengurusan += '<option value="' + valueOfElement.id_jenis + '">' + valueOfElement.jenis +
            '</option>';
        });

        $('#id_jenis_pengurus').append(kepengurusan);
        $("#id_jenis_pengurus").val('34');
      },
      "json"
    );

    let anggota = $('input[name="jenisanggota"]:checked').val();
    if (anggota == 2) {
      $("#id_jenis_pengurus").attr('readonly', true);
    } else {
      $("#id_jenis_pengurus").attr('readonly', false);
    }

    $('input[name="jenisanggota"]').change(function () {
      var value = $('input[name="jenisanggota"]:checked').val();
      if (value == 2) {
        $("#id_jenis_pengurus").val('34');
        $("#id_jenis_pengurus").attr('readonly', true);
      } else {
        $("#id_jenis_pengurus").val('');
        $("#id_jenis_pengurus").attr('readonly', false);
      }
    });

    $.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi', function (data) {
      provinsi += '<option value="">Pilih Provinsi</option>';
      $.each(data, function (index, val) {
        $.each(val, function (index, el) {
          provinsi += '<option data-id="' + el.id + '" value="' + el.nama + '">' + el.nama +
            '</option>';
        });
      });
      $('#provinsi').html(provinsi);
    });

    $('#provinsi').on('change', function (event) {
      let id = $("#provinsi option:selected").attr('data-id');

      $.get('http://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=' + id, function (data) {
        kabupaten += '<option value="">Pilih kabupaten</option>';
        $.each(data, function (index, val) {
          $.each(val, function (index, el) {
            kabupaten += '<option data-id="' + el.id + '" value="' + el.nama + '">' + el.nama +
              '</option>';
          });
        });
        $('#kabupaten').html(kabupaten);
      });
    });

    $('#kabupaten').on('change', function (event) {
      let id = $("#kabupaten option:selected").attr('data-id');

      $.get('https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=' + id, function (data) {
        kecamatan += '<option value="">Pilih kecamatan</option>';
        $.each(data, function (index, val) {
          $.each(val, function (index, el) {
            kecamatan += '<option data-id="' + el.id + '" value="' + el.nama + '">' + el.nama +
              '</option>';
          });
        });
        $('#kecamatan').html(kecamatan);
      });
    });
    $('#kecamatan').on('change', function (event) {
      let id = $("#kecamatan option:selected").attr('data-id');

      $.get('https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=' + id, function (data) {
        kecamatan += '<option value="">Pilih kecamatan</option>';
        $.each(data, function (index, val) {
          $.each(val, function (index, el) {
            kelurahan += '<option data-id="' + el.id + '" value="' + el.nama + '">' + el.nama +
              '</option>';
          });
        });
        $('#kelurahan').html(kelurahan);
      });
    });

  });

  function onlyNumberKey(evt) {

    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
      return false;
    return true;
  }
</script>

<?php if ($this->session->flashdata('success')): ?>
<script>
  Swal.fire({
    title: 'sukses',
    text: "Anda Berhasil Registrasi, Silahkan Login untuk melakukan proses verifikasi",
    icon: 'success',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'LOGIN'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "<?=base_url()?>user/login"
    }
  })
</script>
<?php endif ?>

<?php if ($this->session->flashdata('error')): ?>
<script>
  Swal.fire({
    title: 'Galat',
    text: "<?=$this->session->flashdata('error')?>, silahkan login untuk melakukan transaksi lainnya",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Login!'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "<?=base_url()?>user/login"
    }
  })
</script>
<?php endif ?>

<?php if ($this->session->flashdata('warning')): ?>
<script>
  Swal.fire({
    title: 'Galat',
    text: "File/foto yang anda upload tidak sesuai aturan",
    icon: 'warning',
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  })
</script>
<?php endif ?>