<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Report Data Pembeli </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline">
                <label class="sr-only" for="inlineFormInputName2">Name</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">FORM</div>
                  </div>
                  <input type="date" class="form-control" id="dari">
                </div>

                <label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">TO</div>
                  </div>
                  <input type="date" class="form-control" id="sampai">
                </div>

                <button type="button" class="btn btn-primary mb-2 cari">Filter</button>
              </form>
            </div>
          </div>
          <hr>
          <div class="table-responsive">
            <table class="table display responsive nowrap" id="tb-report" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Cust</th>
                  <th>Nama Lengkap</th>
                  <th>NIK KTP</th>
                  <th>Alamat</th>
                  <th>Provinsi</th>
                  <th>Kabupaten</th>
                  <th>Kecamatan</th>
                  <th>Telepon</th>
                  <th>Tgl Daftar</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>No</td>
                  <td>jenis Cust</td>
                  <td>Nama Lengkap</td>
                  <td>NIK KTP</td>
                  <td>Alamat</td>
                  <td>Provinsi</td>
                  <td>Kabupaten</td>
                  <td>Kecamatan</td>
                  <td>Telepon</td>
                  <td>Tgl Daftar</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  let base_url ='<?=base_url()?>';
  $(function() {
    let table=$('#tb-report').DataTable({
      stateSave: true,
      destroy: true,
      "paging":   true,
      "deferRender": true,
      responsive: true,
      "lengthMenu": [[25, 50, 100, 500, 1000],[25, 50, 100, 500, "Max"]],
      "pageLength": [50],
      ajax: {
        url: base_url+"admin/laporan/datatablepembeli",
        type: "GET",
        "data": function ( data ) {
          data.dari = $('#dari').val();
          data.sampai = $('#sampai').val();
        },
        dataSrc: "",
      },

      columns: [
      {
        render: function (data, type, row, meta) {
          return meta.row + 1;
        },
      },
      {
        data:'jenis_pembeli',render:function(d) {
          return (d==1? 'Penangkar':'Petani')
        }
      },
      { data: 'nama_lengkap'},
      { data: 'nik_ktp' },
      { data: 'telepon' },
      { data: 'alamat_tinggal' },
      { data: 'provinsi' },
      { data: 'kabupaten' },
      { data: 'kecamatan' },
      { data: 'created_at' },
      ],

      dom: 'Bfrtip',
      buttons: [
      {
       extend: 'pdfHtml5',
       orientation: 'potrait',
       pageSize: 'LEGAL',
       messageTop:"LAPORAN DATA PEMBELI"
     }
     ]

    });
    $('.cari').on('click', function(event) {
      event.preventDefault();
      table.ajax.reload();
    });
  });
</script>