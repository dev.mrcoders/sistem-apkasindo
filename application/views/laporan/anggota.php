<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Report Data Anggota </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline">
                <label class="sr-only" for="inlineFormInputName2">Name</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">FORM</div>
                  </div>
                  <input type="date" class="form-control" id="dari">
                </div>

                <label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">TO</div>
                  </div>
                  <input type="date" class="form-control" id="sampai">
                </div>

                <button type="button" class="btn btn-primary mb-2 cari">Filter</button>
              </form>
            </div>
          </div>
          <hr>
          <div class="table-responsive">
            <table class="table display responsive nowrap" id="tb-report" width="100%">
              <thead>
                <tr>
                  <th>Nomor Anggota</th>
                  <th>Ke Anggotaan</th>
                  <th>Nama Lengkap</th>
                  <th>NIK KTP</th>
                  <th>Telepon</th>
                  <th>Alamat</th>
                  <th>RT RW</th>
                  <th>Provinsi</th>
                  <th>Kabupaten</th>
                  <th>Kecamatan</th>
                  <th>Kelurahan</th>
                  <th>Status</th>
                  <th>Tgl Daftar</th>
                </tr>
              </thead>
              <tbody>
                <tr>

                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  let base_url ='<?=base_url()?>';
  $(function() {
    let table=$('#tb-report').DataTable({
      stateSave: true,
      destroy: true,
      "paging":   true,
      "deferRender": true,
      responsive: true,
      "lengthMenu": [[25, 50, 100, 500, 1000],[25, 50, 100, 500, "Max"]],
      "pageLength": [50],
      ajax: {
        url: base_url+"admin/laporan/datatablependaftar",
        type: "GET",
        "data": function ( data ) {
          data.dari = $('#dari').val();
          data.sampai = $('#sampai').val();
        },
        dataSrc: "",
      },

      columns: [
      {
        data:'nomor_anggota'
      },
      {
        data:'jenis_anggota',render:function(d) {
          return (d==1? 'Anggota':'Pengurus')
        }
      },
      { data: 'nama_lengkap'},
      { data: 'nik_ktp' },
      { data: 'telepon' },
      { data: 'alamat_tinggal' },
      { data: {
        rt:"rt",
        rw:"rw"
      },render:function(d){
        return 'RT '+d.rt+' RW '+d.rw;
      }},
      { data: 'provinsi' },
      { data: 'kabupaten' },
      { data: 'kecamatan' },
      { data: 'kelurahan' },
      { data: 'status',render:function(d) {
        if(d==1){
          return '<span class="badge badge-success">Verified</span>';
        }else{
          return '<span class="badge badge-warning">Un Verified</span>';
        }
      } },
      { data: 'created_at' },
      ],

      dom: 'Bfrtip',
      buttons: [
      {
       extend: 'pdfHtml5',
       orientation: 'landscape',
       pageSize: 'LEGAL'
     }
     ]

    });
    $('.cari').on('click', function(event) {
      event.preventDefault();
      table.ajax.reload();
    });
  });
</script>