<div class="content-wrapper">
  <div class="row">
    <div class="col-sm-12 mb-4 mb-xl-0">
      <h4 class="font-weight-bold text-dark">SELAMAT DATANG DI SISTEM APKASINDO</h4> 
    </div>
  </div>
  <?php if ($this->session->userdata('level')== 1 || $this->session->userdata('level')== 2 || $this->session->userdata('level')== 6): ?>
  <div class="row mt-3">
    <div class="col-xl-12 flex-column d-flex grid-margin stretch-card">
      <div class="row flex-grow">
        <?php 
        if($this->session->userdata('level')== 1){
          $show=['','','','',''];
          $col='col-sm-12';
          $text_title='';
        }elseif($this->session->userdata('level')== 2){
          $show=['','hidden','hidden','','hidden'];
          $col='col-sm-8';
          $text_title='';
        }else{
          $show=['','','','hidden',''];
          $col='col-sm-12';
          $text_title='Pembeli';
        }
        ?>

        <div class="col-sm-4  stretch-card" <?=$show[0]?> >
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"><?=$text_title?> Keanggotaan</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <p>Pengurus</p>
                  <h4 class="text-dark font-weight-bold mb-2 pengurus">43,981</h4>
                </div>
                <div class="col-sm-6">
                  <p>Anggota</p>
                  <h4 class="text-dark font-weight-bold mb-2 anggota">43,981</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4  stretch-card" <?=$show[1]?>>
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"><?=$text_title?> Non Anggota</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <p>Penangkar</p>
                  <h4 class="text-dark font-weight-bold mb-2 penangkar">43,981</h4>
                </div>
                <div class="col-sm-6">
                  <p>Petani Non Anggota</p>
                  <h4 class="text-dark font-weight-bold mb-2 petani">43,981</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4  stretch-card" <?=$show[2]?>>
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Transaksi Bibit</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <p>Unpaid</p>
                  <h4 class="text-dark font-weight-bold mb-2 unpaid">43,981</h4>
                </div>
                <div class="col-sm-4">
                  <p>Proses</p>
                  <h4 class="text-dark font-weight-bold mb-2 proses">43,981</h4>
                </div>
                <div class="col-sm-4">
                  <p>Success</p>
                  <h4 class="text-dark font-weight-bold mb-2 sukses">43,981</h4>
                </div>
              </div>
            </div>
          </div>
        </div>  
        <div class="<?=$col?>  stretch-card mt-2" <?=$show[3]?>>
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Pendaftar</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <p>Total Pendaftar</p>
                  <h4 class="text-dark font-weight-bold mb-2 total">43,981</h4>
                </div>
                <div class="col-sm-4">
                  <p>Accepted</p>
                  <h4 class="text-dark font-weight-bold mb-2 accepted">43,981</h4>
                </div>
                <div class="col-sm-4">
                  <p>Rejected</p>
                  <h4 class="text-dark font-weight-bold mb-2 rejected">43,981</h4>
                </div>
              </div>
            </div>
          </div>
        </div> 

      </div>
    </div>

    <div class="col-xl-12 d-flex stretch-card" >
      <div class="card" <?=$show[4]?>>
        <div class="card-body">
          <h4 class="card-title">Transaksi Bibit</h4>
          <div class="row">
            <div class="col-lg-5">
            </div>
            <div class="col-lg-7">
              <div class="chart-legends d-lg-block d-none" id="chart-legends"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <canvas id="myChart" class="mt-3"></canvas>
            </div>
          </div>

        </div>
      </div>
    </div> 
  </div>
<?php endif ?>
<?php if ($this->session->userdata('level')== 3 || $this->session->userdata('level')== 5): ?>
  <div class="card-body">
  <div class="col-sm-12">
                <h4>Identitas Pembeli</h4>
                <hr>
                <div class="form-group row mb-0">
                  <label for="exampleInputUsername2" class="col-sm-5 col-form-label">Nama Lengkap</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->nama_lengkap?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputEmail2" class="col-sm-5 col-form-label">NIK KTP</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->nik_ktp?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputEmail2" class="col-sm-5 col-form-label">Alamat Tinggal</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->alamat_tinggal?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Kecamatan</label>
                  <div class="col-sm-7 ">
                    <input type="text" class="form-control" name="" value="<?=$data->kecamatan?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Kabupaten</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->kabupaten?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">Provinsi</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->provinsi?>" readonly>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <label for="exampleInputMobile" class="col-sm-5 col-form-label">No Handphone</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="" value="<?=$data->telepon?>" readonly>
                  </div>
                </div>
              </div>
  <div class="row">
           
<?php endif ?>

<?php if ($this->session->userdata('level')== 4): ?>
  <?php if ($isverif->status == 0): ?>
    <div class="card text-center">
      <div class="card-header">
        Status Verifikasi Akun
      </div>
      <div class="card-body">
        <h5 class="card-title">Status Akun: <label for="" class="badge badge-info">on proses</label></h5>
        <?php if ($isverif->bukti_bayar == NULL): ?>
          <h5 class="card-title">Bukti bayar: <label for="" class="badge badge-danger">No Upload</label></h5>
          <p>Silahkan Upload Bukti Pembayaran Agar Akun Dapat diverifikasi</p>
          <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-buktibayar" data-backdrop="static" data-keyboard="false">Upload Bukti bayar</a>
        <?php else: ?>
          <h5 class="card-title">Bukti bayar: <label for="" class="badge badge-success"><?=$isverif->bukti_bayar?></label></h5>
        <?php endif ?>

        <br>
        <br>
        <p class="card-text">Akun Akan Terverifikasi Paling Lambat  </p>
        <strong><?=date('d F Y',strtotime('+3 days',strtotime($data->created_at)))?> </strong>
        <br>
        <br>
        <br>
        <p>
          Jika Sampai Batas Waktu Belum Terverifikasi <br> Silahkan Hubungi Admin Di 
          <br><strong>0823983495</strong>
        </p>

      </div>
      <div class="card-footer text-muted">
      </div>
    </div>

    <div class="modal fade" id="form-buktibayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Upload Bukti Pembayaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?=base_url()?>user/registrasi/uploadbuktibayar" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="row py-4">
                <div class="col-lg-12 ">
                  <input type="hidden" name="id_verif" value="<?=$isverif->id_verif?>">
                  <!-- Upload image input-->
                  <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                    <input id="upload" type="file" onchange="" name="bukti_bayar" class="form-control border-0">
                    <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file </label>
                    <div class="input-group-append">
                      <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> <i class="fa fa-upload mr-2 text-muted"></i><small class="text-uppercase font-weight-bold text-muted">Choose file</small></label>
                    </div>
                  </div>
                  <small class="ml-2 text-danger"> *jpg,jpeg,png max size 5 Mb</small>
                  <small class="ml-2 " id="output"> </small>

                  <!-- Uploaded image area-->
                  <p class="font-italic text-white text-center">The image uploaded will be rendered inside the box below.</p>
                  <div class="image-area mt-4"><img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Upload File</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php elseif($isverif->status == 2): ?>
    <div class="card text-center">
      <div class="card-header">
        Status Verifikasi Akun
      </div>
      <div class="card-body">
        <h5 class="card-title">Status Akun: <label for="" class="badge badge-danger">Di Tolak</label></h5>
        <?php if ($isverif->bukti_bayar == NULL): ?>
          <h5 class="card-title">Bukti bayar: <label for="" class="badge badge-danger">No Upload</label></h5>
          <p>Silahkan Upload Bukti Pembayaran Agar Akun Dapat diverifikasi</p>
          <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-buktibayar" data-backdrop="static" data-keyboard="false">Upload Bukti bayar</a>
        <?php else: ?>
          <h5 class="card-title">Bukti bayar: <label for="" class="badge badge-success"><?=$isverif->bukti_bayar?></label></h5>
          <div class="alert alert-info">
            <b>Keterangan Penolakan :</b> <p><?=$isverif->keterangan?></p>
            <a href="<?=base_url()?>user/dashboard/updatedata" class="btn btn-warning btn-sm">Perbarui Data</a>
          </div>
        <?php endif ?>

        <br>
        <br>
        <p class="card-text">Akun Akan Terverifikasi Paling Lambat  </p>
        <strong><?=date('d F Y',strtotime('+3 days',strtotime($data->created_at)))?> </strong>
        <br>
        <br>
        <br>
        <p>
          Jika Sampai Batas Waktu Belum Terverifikasi <br> Silahkan Hubungi Admin Di 
          <br><strong>0823983495</strong>
        </p>

      </div>
      <div class="card-footer text-muted">
      </div>
    </div>
  <?php else: ?>
    <?php include 'profil.php'; ?>
  <?php endif ?>
<?php endif ?>


</div>

<script>
  const base_url = "<?=base_url()?>";
  function readURL(input) {
    var filePath = input.value;
    var allowedExtensions =/(\.jpg|\.jpeg|\.png|\.)$/i;
    if (!allowedExtensions.exec(filePath)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Type File salah, silahkan pilih ulang',
      }).then((result) => {
        if (result.isConfirmed) {
          input.value = '';
          return false;
        }
      })
      
    }else{
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imageResult')
          .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }
  }

  $(function () {
    $('#upload').on('change', function () {
      const size =(this.files[0].size / 1024 / 1024).toFixed(2);
      if (size > 5) {
        
        $("#output").html('<b>' +
          'This file size is: ' + size + " MB" + '</b>');

      }else{
        readURL(this);
      }

    });
  });

  $(function() {
    var xValues = ['JAN','FEB','MAR','MEI','JUN','JUL','AGUS','SEPT','OKT','NOV','DES'];
    $.ajax({
      url: base_url+'dashboard/getdataanggota',
      type: 'get',
      success: function (data) {
        $('.anggota').text(data.anggota);
        $('.pengurus').text(data.pengurus);
        $('.penangkar').text(data.penangkar);
        $('.petani').text(data.petani);
        $('.unpaid').text(data.unpaid);
        $('.proses').text(data.prosess);
        $('.sukses').text(data.success);
        $('.accepted').text(data.accepted);
        $('.rejected').text(data.rejected);
        $('.total').text(data.total);
      }
    });

    new Chart("myChart", {
      type: "line",
      data: {
        labels: xValues,
        datasets: [{ 
          label: 'success',
          data: [860,1140,1060,1060,1070,1110,1330,2210,7830,2478,8000],
          borderColor: "green",
          fill: false
        }, ]
      },
      options: {
        legend: {display: true}
      }
    });
  });
</script>