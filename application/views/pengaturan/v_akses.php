<div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                 
                  <div class="table-responsive">
                    <table class="table" id="tb-id">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Role</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data as $d => $v): ?>
                        <tr>
                          <td><?= $d + 1 ?></td>
                          <td><?= $v->nama_level ?></td>
                          <td>
                            <?php if ($v->aktif == 1): ?>
                              <label class="badge badge-success">Aktif</label>
                            <?php else: ?>
                              <label class="badge badge-danger">non aktif</label>
                            <?php endif; ?>
                          </td>
                          <td>
                            <a href="<?=base_url()?>pengaturan/akses/edit/<?=$v->id_level?>" class="btn btn-warning btn-sm">Edit</a>
                          </td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
<script>
    $(document).ready(function() {
    $('#tb-id').DataTable( {
        // order: [[0, 'asc'], [1, 'asc']],
        // rowGroup: {
        //     dataSrc: [ 0,1 ]
        // },
        // columnDefs: [ {
        //     targets: [ 0,1 ],
        //     visible: false
        // } ]
    } );
} );
</script>