<div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="tb-id">
                      <thead>
                        <tr class="table-info">
                          <th>#</th>
                          <th>Main Menu</th>
                          <th>Sub Menu</th>
                          <th>Akses</th>
                          <th>C</th>
                          <th>R</th>
                          <th>U</th>
                          <th>D</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($data as $i=>$d): ?>
                            <?php $sub = $this->db->get_where('tb_submenu',['menu_id'=>$d->id_menu]); ?>
                            <tr>
                            <?php if($d->url == "#"): ?>
                                <td><?=$i+1?></td>
                                <?php if($sub->num_rows()<0): ?>
                                    <td colspan="2">bb</td>
                                <?php else: ?>
                                    <td rowspan="2">aab</td>

                                <?php endif ?>
                            <?php else: ?>
                                    <td><?=$i+1?></td>
                                    <td colspan="2"><?=$d->nama_menu?></td>
                                    <td>
                                        <label class="toggle-switch">
                                            <input type="checkbox" checked>
                                            <span class="toggle-slider round"></span>
                                        </label>
                                    </td>
                                    <td >
                                        <label class="toggle-switch">
                                            <input type="checkbox" checked>
                                            <span class="toggle-slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <label class="toggle-switch">
                                            <input type="checkbox" checked>
                                            <span class="toggle-slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <label class="toggle-switch">
                                            <input type="checkbox" checked>
                                            <span class="toggle-slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <label class="toggle-switch">
                                            <input type="checkbox" checked>
                                            <span class="toggle-slider round"></span>
                                        </label>
                                    </td>
                            <?php endif ?>
                            </tr>
                        <?php endforeach ?>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>