<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
    private $path = 'master/';
    private $template = "template/index";
    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {
        $data['title']='';
        $data['form']=$this->Formulir('anggota');
        $data['data']=$this->db->get('v_anggota_aktif')->result_array();
        $data['page']=$this->path.'v_anggota';
        $this->load->view($this->template,$data);
    }

    public function NewData($value='')
    {
        $data['title']='';
        $data['form']=$this->Formulir('anggota');
        $data['page']=$this->path.'form_anggota';
        $this->load->view($this->template,$data);
    }

    public function Datatable($value='')
    {
        $this->db->select('v_anggota_aktif.*,tb_user.username');
        $this->db->join('tb_user','tb_user.id_user=v_anggota_aktif.user_id','inner');
        $result = $this->db->get('v_anggota_aktif')->result();

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function StoreAnggota()
    {
        $config['upload_path']          = './uploads/'.$this->input->post('username').'/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';

        $this->load->library('upload', $config);
        $this->MakeDirectory($config['upload_path']);

        
        if ($this->input->post('jenisanggota')) {
            $level = 4;
        }elseif ($this->input->post('jenispembeli')) {
            $level = ($this->input->post('jenispembeli') == 1 ? 5:3);
        }
        
        $InsertUser = [
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'level'=>$level
        ];


        $GetFieldForm = $this->Formulir('anggota');

        $fi =[];
        $file=[];
        foreach ($GetFieldForm as $i => $v) {
            if ($v->type == 'file') {
                $file[]=$v->name;
            }
            $fi[$v->name]=$this->input->post($v->name);
        }

        foreach ($file as $image) {
            if ( ! $this->upload->do_upload($image))
            {
                $this->session->set_flashdata('warning', 'galat');
                redirect(base_url().'master/anggota/newdata');
            }
            else
            {
                $data=$this->upload->data();
                $fi[$image]=$data['file_name'];
            }

        }
        $this->db->insert('tb_user',$InsertUser);
        $id_user = $this->db->insert_id();
        
        $fi['nomor_anggota']=$this->input->post('no_anggota');
        $fi['jenis_anggota']=$this->input->post('jenisanggota');
        $fi['user_id']=$id_user;
        $fi['created_at']=date('Y-m-d H:i:s');

        if ($this->IsNikTerdaftar($this->input->post('nik_ktp'))) {
            $this->session->set_flashdata('error', 'NIK KTP anda telah terdaftar sebagai anggota APKASINDO');
            redirect(base_url().'formulir-pendaftaran','refresh');
        }else{
            $this->db->insert('tb_anggota',$fi);
            $id_anggota = $this->db->insert_id();

            $Insert_verif_data = [
                'anggota_id'=>$id_anggota,
                'status'=>1,
                'created_at'=>NULL,
                'is_pendaftar'=>'N'
            ];
            $this->db->insert('tb_anggota_verif',$Insert_verif_data);
            $this->session->set_flashdata('success', 'Data Berhasil Di Tambahkan');
            redirect(base_url().'master/anggota','refresh');
        }
    }

    public function IsNikTerdaftar($nik_ktp)
    {
        $GetDataAnggota = $this->db->get_where('v_anggota_aktif', ['nik_ktp'=>$nik_ktp]);
        $GetDataPembeli = $this->db->get_where('tb_pembeli', ['nik_ktp'=>$nik_ktp]);
        if ($GetDataAnggota->num_rows() > 0) {
            return true;
        }elseif($GetDataPembeli->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function MakeDirectory($path)
    {
        if (!is_dir($path)) {
            return mkdir($path, 0777, true);
        }
    }

    public function Formulir($type)
    {
        $form=[];

        if ($type =='anggota') {
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','RT','RW','Provinsi','Kabupaten','Kecamatan','Kelurahan','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 14? 'rata_rata_produksi':($i == 16? 'telepon':$lb))));
                $type = ($i == 11 || $i == 17 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }else{
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Provinsi','Kabupaten','Kecamatan','Nomor HP','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 6? 'telepon':$lb)));
                $type = ($i == 7 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }
        
        $obj = json_decode(json_encode($form));

        return $obj;

    }

    public function pembeli($value='')
    {
        $data['title']='';
        $this->db->order_by('id_pembeli', 'desc');
        $data['form']=$this->Formulir('p');
        $data['data'] = $this->db->get('tb_pembeli')->result_array();
        $data['page']=$this->path.'v_pembeli';
        $this->load->view($this->template,$data);

    }

    public function delete($va,$id)
    {
        if ($va == 'a') {
            $this->db->where('id_anggota', $id);
            $this->db->delete('tb_anggota');
            redirect(base_url().'master/anggota','refresh');
        }
        if ($va == 'p') {
            $this->db->where('id_pembeli', $id);
            $this->db->delete('tb_pembeli');
            redirect(base_url().'master/anggota/pembeli','refresh');
        }

    }


}
