<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	private $template = "template/index";

	public function index($url='')
	{
		
		$data['title']='';
		$this->db->join('tb_level b', 'a.level=b.id_level', 'inner');
		if ($url) {
			$this->db->where_in('b.id_level', [3,4,5]);
		}else{
			$this->db->where_in('b.id_level', [1,2,6]);
		}
		
		$data['data']=$this->db->get('tb_user a');
		$data['level']=$this->db->get('tb_level');
		$data['page']='pengguna/admin';
		$this->load->view($this->template,$data);
	}


	public function CreateUser()
	{
		$InsUser=[
			'username'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password')),
			'level'=>$this->input->post('level'),
		];

		$this->db->insert('tb_user', $InsUser);
		redirect(base_url().'admin/user');
	}

	public function UpdateUser()
	{
		$InsUser=[
			'username'=>$this->input->post('username'),
			'level'=>$this->input->post('level'),
		];
		if ($this->input->post('password')) {
			$InsUser['password']=md5($this->input->post('password'));
		}

		$this->db->where('id_user', $this->input->post('id_user'));
		$this->db->update('tb_user', $InsUser);
		redirect(base_url().'admin/user');

	}


}

/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */