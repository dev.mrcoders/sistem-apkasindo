<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	private $template = "template/index";
	public function index()
	{
		$data['title']='';
		$data['page']='laporan/anggota';
		$this->load->view($this->template,$data);
	}

	public function DataTablePendaftar($value='')
	{
		if ($this->input->get('dari') && $this->input->get('sampai')) {
			$this->db->where('created_at BETWEEN "'.$this->input->get('dari').'" AND "'.$this->input->get('sampai').'"');
		}
		$hasil = $this->db->get('v_new_anggota')->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($hasil));
	}

	public function Pembeli()
	{
		$data['title']='';
		$data['page']='laporan/pembeli';
		$this->load->view($this->template,$data);
	}

	public function DataTablePembeli($value='')
	{
		if ($this->input->get('dari') && $this->input->get('sampai')) {
			$this->db->where('created_at BETWEEN "'.$this->input->get('dari').'" AND "'.$this->input->get('sampai').'"');
		}
		$hasil = $this->db->get('tb_pembeli')->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($hasil));
	}

	public function Transaksi()
	{
		$data['title']='';
		$data['page']='laporan/transaksi';
		$this->load->view($this->template,$data);
	}

	public function DataTableTransaksi($value='')
	{
		if ($this->input->get('dari') && $this->input->get('sampai')) {
			$this->db->where('created_at BETWEEN "'.$this->input->get('dari').'" AND "'.$this->input->get('sampai').'"');
		}
		$this->db->join('v_transaksi_bibit b', 'a.user_id=b.user_id', 'inner');
		$hasil = $this->db->get('v_pembeli_transaksi a')->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($hasil));
	}

}

/* End of file Laporan.php */
/* Location: ./application/controllers/admin/Laporan.php */