<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	private $template = "template/index";
	public function index()
	{
		$data['title']='';
		$data['data']=$this->db->get('v_transaksi_bibit')->result();
		$data['page']='transaksi/data';
		$this->load->view($this->template,$data);
	}

	public function Invoice($kd)
	{
		$data['title']='';
		$data['transaksi']=$this->db->get_where('tb_transaksi',['kode_transaksi'=>$kd])->row();
		$data['user']=$this->db->get_where('v_pembeli_transaksi', ['user_id'=>$data['transaksi']->user_id])->row();
		$data['invoice']=$this->db->get_where('tb_transaksi_invoice', ['kode_transaksi'=>$kd])->row();

		$data['page']='transaksi/invoice';
		$this->load->view($this->template,$data);
		
	}

	public function verifikasi($id_invoice,$status_beli)
	{
		$statusUpdate=[
			'status_pembayaran'=>'success',
			'status_beli'=>$status_beli
		];
		$this->db->where('kode_invoice', $id_invoice);
		$this->db->update('tb_transaksi_invoice', $statusUpdate);
		redirect(base_url().'admin/transaksi','refresh');
	}

	public function Payment()
	{
		$this->db->join('tb_transaksi b', 'a.id_user=b.user_id', 'inner');
		$Getpath=$this->db->get_where('tb_user a',['b.kode_transaksi'=>$this->input->post('kd_transaksi')])->row()->username;
		$config['upload_path'] = './uploads/'.$Getpath.'/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['file_name'] = 'buktitranser-'.time();

		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('bukti_bayar')){
			$data =  $this->upload->display_errors();
			redirect(base_url().'admin/transaksi/invoice/'.$this->input->post('kd_transaksi'),'refresh');
		}
		else{
			$data =  $this->upload->data();
			$InsPayment = [
				'nama_rekening'=>'On Cash Manual Pay',
				'jumlah_bayar'=>$this->input->post('jumlah_bayar'),
				'jenis_pembayaran'=>'cash',
				'status_pembayaran'=>'success',
				'bukti_pembayaran'=>$data['file_name'],
				'file_path'=>$data['file_path'],
				'tgl_bayar'=>date('Y-m-d H:m:s')
			];
			$this->db->where('kode_invoice', $this->input->post('id_invoice'));
			$this->db->update('tb_transaksi_invoice', $InsPayment);

			redirect(base_url().'admin/transaksi','refresh');

		}
		// $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}



}

/* End of file Transaksi.php */
/* Location: ./application/controllers/admin/Transaksi.php */