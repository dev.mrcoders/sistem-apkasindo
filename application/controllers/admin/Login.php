<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    private $template = "user/login";
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title'] = '';
        $data['path_checking'] = 'admin/login/checklogin';
        $this->load->view($this->template, $data);
    }

    public function CheckLogin($type = null)
    {
        if ($this->input->post('captcha') ==  $this->session->userdata('captchaword')) {
            $Post_login = [
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
            ];
            //menampung request
            $ChekingUser = $this->db->get_where('tb_user', $Post_login, 1)->row();
            if ($ChekingUser) {
                $newdata = array(
                    'user_id'   => $ChekingUser->id_user,
                    'username'  => $ChekingUser->username,
                    'level'     => $ChekingUser->level,
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($newdata);
                redirect(base_url() . 'dashboard');
            } else {
                $this->session->set_flashdata('error', 'Username Atau Password Salah');
                if ($type != null) {
                    
                    redirect(base_url() . 'user/login');
                } else {
                    redirect(base_url() . 'admin/login');
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Chaptcha tidak sesuai');
            if ($type != null) {
                
                redirect(base_url() . 'user/login');
            } else {
                redirect(base_url() . 'admin/login');
            }
        }
    }

    public function LogOut()
    {
        $data_session = array('user_id' => "", 'username' => "", 'level' => "", 'logged_in' => FALSE);
        // if ($this->session->userdata('level')==1 || $this->session->userdata('levev')==2) {
        //     $this->session->unset_userdata($data_session);//clear session
        //     $this->session->sess_destroy();//tutup session
        //     redirect(base_url().'admin/login');
        // }else{
        $this->session->unset_userdata($data_session); //clear session
        $this->session->sess_destroy(); //tutup session
        redirect(base_url() . 'user/login');
        // }

    }
}
