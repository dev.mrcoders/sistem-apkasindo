<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    private $template = "user/index";
    private $vpath = "user/";
    public function __construct()
    {
        parent::__construct();
                
    }
	public function index()
	{
        $data['title']='';
        $data['page']=$this->vpath.'home';
		$this->load->view($this->template,$data);
	}
}
