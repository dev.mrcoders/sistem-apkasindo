<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kta extends CI_Controller {
	private $template = "template/index";
	public function index()
	{
		$data['title']='';
		$data['data']=$this->db->get_where('tb_anggota',['user_id'=>$this->session->userdata('user_id')])->row();
		$data['page']='user/mykta';
		$this->load->view($this->template,$data);
	}

}

/* End of file Kta.php */
/* Location: ./application/controllers/user/Kta.php */