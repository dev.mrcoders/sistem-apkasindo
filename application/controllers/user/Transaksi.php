<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	private $ulevel;
	private $uid;
	private $uname;
	private $template = "template/index";

	public function __construct()
	{
		parent::__construct();
		$this->telegram = new Telegram\Bot\Api('5800933300:AAFp_1eGZXUGSZvaCmB6cHYKjfbVdNgOOzo');
		$this->ulevel = $this->session->userdata('level');
		$this->uid = $this->session->userdata('user_id');
		$this->uname=$this->session->userdata('username');
	}

	public function index($new='')
	{
		$data['title']='';
		if ($this->ulevel == 3 || $this->ulevel == 5) {
			$data['data']=$this->db->get_where('tb_pembeli', ['user_id'=>$this->uid])->row();
		}else{
			$data['data']=$this->db->get_where('tb_anggota', ['user_id'=>$this->uid])->row();
		}
		$data['isTransasksi']=$this->db->get_where('v_transaksi_bibit',['user_id'=>$this->session->userdata('user_id')]);
		$data['isAda']=($new !='' ? 0:$data['isTransasksi']->num_rows());
		$data['isFile']=$this->db->get_where('v_file_akses', ['level_id'=>$this->ulevel])->result();
		$data['page']='user/transaksi';
		$this->load->view($this->template,$data);
		// $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function CreateTransaksi()
	{
		$config['upload_path'] = './uploads/'.$this->uname.'/';
		$config['allowed_types'] = 'gif|jpg|pdf|jpeg|png';

		
		$this->load->library('upload', $config);

		$InsTransaksi = [
			'kode_transaksi'=>$this->KodeTransaksi(),
			'user_id'=>$this->uid,
			'jumlah_bibit'=>$this->input->post('jml_bibit'),
			'total_harga'=>$this->input->post('total_beli')
		];

		$IsFile=$this->db->get_where('v_file_akses', ['level_id'=>$this->ulevel])->result();
		foreach($IsFile as $i=>$file){
			if ( ! $this->upload->do_upload($file->nama_file)){
				// $response = array('error' => $this->upload->display_errors());
			}
			else{
				$dfile= $this->upload->data();
				$InsFile[]=[
					'file_id'=>$file->id_file,
					'user_id'=>$this->uid,
					'kd_transaksi'=>$InsTransaksi['kode_transaksi'],
					'file'=>$dfile['file_name'],
					'file_path'=>$dfile['file_path'],
					'created_at'=>date('Y-m-d H:i:s')
				];
			}
		}
		$KdInvoice = $this->randoma();
		$checkIdInvoice = $this->db->get_where('tb_transaksi_invoice',['kode_invoice'=>$KdInvoice]);
		if ($checkIdInvoice->num_rows()<0) {
			$KdInvoice = $this->randoma();
		}

		$InsertInvoice =[
			'kode_invoice'=>$KdInvoice,
			'kode_transaksi'=>$InsTransaksi['kode_transaksi'],
			'jumlah_bayar'=>'',
			'jenis_pembayaran'=>NULL,
			'tgl_bayar'=>NULL,
			'status_pembayaran'=>'unpaid',
			'bukti_pembayaran'=>'',
			'file_path'=>'',
			'created_at'=>date('Y-m-d H:i:s'),
			'nama_rekening'=>'',
			'keterangan'=>''
		];



		$this->db->insert_batch('tb_fileupload', $InsFile);
		$this->db->insert('tb_transaksi', $InsTransaksi);
		$this->db->insert('tb_transaksi_invoice', $InsertInvoice);
		


		redirect(base_url().'user/transaksi/invoice/'.$InsTransaksi['kode_transaksi'],'refresh');
		
	}

	public function Invoice($kd)
	{
		if ($this->ulevel == 3 || $this->ulevel == 5) {
			$data['user']=$this->db->get_where('tb_pembeli', ['user_id'=>$this->uid])->row();
		}else{
			$data['user']=$this->db->get_where('tb_anggota', ['user_id'=>$this->uid])->row();
		}
		$data['transaksi']=$this->db->get_where('tb_transaksi',['kode_transaksi'=>$kd])->row();
		$data['invoice']=$this->db->get_where('tb_transaksi_invoice', ['kode_transaksi'=>$kd])->row();

		$data['page']='user/invoice';
		$this->load->view($this->template,$data);
		
	}

	public function download($kd)
	{
		if ($this->ulevel == 3 || $this->ulevel == 5) {
			$data['user']=$this->db->get_where('tb_pembeli', ['user_id'=>$this->uid])->row();
		}else{
			$data['user']=$this->db->get_where('tb_anggota', ['user_id'=>$this->uid])->row();
		}
		$data['transaksi']=$this->db->get_where('tb_transaksi',['kode_transaksi'=>$kd])->row();
		$data['invoice']=$this->db->get_where('tb_transaksi_invoice', ['kode_transaksi'=>$kd])->row();

		$data['page']='user/download';
		$this->load->view($this->template, $data);
	}

	public function Payment()
	{

		$config['upload_path'] = './uploads/'.$this->uname.'/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['file_name'] = 'buktitranser-'.time();

		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('bukti_bayar')){
			$data =  $this->upload->display_errors();
			redirect(base_url().'user/transaksi/invoice/'.$this->input->post('kd_transaksi'),'refresh');
		}
		else{
			$data =  $this->upload->data();
			$InsPayment = [
				'nama_rekening'=>$this->input->post('nama_rekening'),
				'jumlah_bayar'=>$this->input->post('jumlah_bayar'),
				'jenis_pembayaran'=>'transfer',
				'status_pembayaran'=>'proses',
				'bukti_pembayaran'=>$data['file_name'],
				'file_path'=>$data['file_path'],
				'tgl_bayar'=>date('Y-m-d H:m:s')
			];
			$this->db->where('kode_invoice', $this->input->post('id_invoice'));
			$this->db->update('tb_transaksi_invoice', $InsPayment);

			$caption ='';
			$caption =$caption.'Nama Pembeli : '.$this->uname.''.PHP_EOL;
			$caption =$caption.'Jumlah Pembelian : '.$this->input->post('jumlah_bibit').''.PHP_EOL;

			$response = $this->telegram->sendPhoto([
				'chat_id' => '956713352', 
				'photo' => base_url().'uploads/'.$this->uname.'/'.$data['file_name'], 
				'caption' => $caption
				
			
			]);

			$response = $this->telegram->sendPhoto([
				'chat_id' => '5280543368', 
				'photo' => base_url().'uploads/'.$this->uname.'/'.$data['file_name'], 
				'caption' => $caption
				
			
			]);

			// $response = $this->telegram->sendPhoto([
			// 	'chat_id' => '1366473690', 
			// 	'photo' => base_url().'uploads/'.$this->uname.'/'.$data['file_name'], 
			// 	'caption' => $caption
				
			
			// ]);
			

			$messageId = $response->getMessageId();
			redirect(base_url().'user/transaksi','refresh');

		}
		// $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	

	public function KodeTransaksi()
	{
		$this->db->select('RIGHT(kode_transaksi,5) as kode_transaksi', FALSE);
		$this->db->where('user_id', $this->uid);
		$this->db->order_by('kode_transaksi','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tb_transaksi');
		if($query->num_rows() <> 0){      
			$data = $query->row();
			$kode = intval($data->kode_transaksi) + 1; 
		}
		else{      
			$kode = 1;  
		}
		$batas = str_pad($kode, 4, "0", STR_PAD_LEFT);    
		$kodetampil = "TRX-".$batas.':'.$this->uid;
		return $kodetampil;  
	}

	function randoma(){
		$a=rand(100,500);
		$b=rand(500,900);

		return $a.'-'.$b;
	}
	

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/user/Transaksi.php */