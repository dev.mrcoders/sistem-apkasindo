<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    private $template = "userlogin";
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('captcha');
    }
    public function index()
    {
        $data['captcha'] = $this->create_captcha();
        $data['path_checking'] = 'admin/login/checklogin/user';
        $this->load->view($this->template, $data);
    }

    function create_captcha()
    {
        $data = array(
            'img_path' => './captcha/',
            'img_url' => base_url('captcha'),
            'img_width' => '300',
            'img_height' => '40',
            'word_length'   => 4,
            'font_size'     => 20,
            'expiration' => 7200,
            'img_id'        => 'Imageid',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );

        $captcha = create_captcha($data);
        $image = $captcha['image'];

        $this->session->set_userdata('captchaword', $captcha['word']);

        return $image;
    }
}
