<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {
    private $template = "user/index";
    private $vpath = "user/";
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {


        $data['form_anggota']=$this->Formulir('anggota');
        $data['form_penangkar']=$this->Formulir('penangkar');
        $data['page']=$this->vpath.'registrasi';
        $this->load->view($this->template,$data);

    }

    public function Formulir($type)
    {
        $form=[];

        if ($type =='anggota') {
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','RT','RW','Provinsi','Kabupaten','Kecamatan','Kelurahan','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 14? 'rata_rata_produksi':($i == 16? 'telepon':$lb))));
                $type = ($i == 11 || $i == 17 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }else{
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Provinsi','Kabupaten','Kecamatan','Nomor HP','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 6? 'telepon':$lb)));
                $type = ($i == 7 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }
        
        $obj = json_decode(json_encode($form));

        return $obj;

    }

    public function StoreRegistrasiKeanggotaan()
    {
        $config['upload_path']          = './uploads/'.$this->input->post('username').'/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';

        $this->load->library('upload', $config);
        $this->MakeDirectory($config['upload_path']);

        
        if ($this->input->post('jenisanggota')) {
            $level = 4;
        }elseif ($this->input->post('jenispembeli')) {
            $level = ($this->input->post('jenispembeli') == 1 ? 5:3);
        }
        
        $InsertUser = [
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'level'=>$level
        ];

        $this->db->insert('tb_user',$InsertUser);
        $id_user = $this->db->insert_id();

        if ($this->input->post('jenisanggota')) {

            $GetFieldForm = $this->Formulir('anggota');

            $fi =[];
            $file=[];
            foreach ($GetFieldForm as $i => $v) {
                if ($v->type == 'file') {
                    $file[]=$v->name;
                }
                $fi[$v->name]=$this->input->post($v->name);
            }

            foreach ($file as $image) {
                if ( ! $this->upload->do_upload($image))
                {
                    $this->session->set_flashdata('warning', 'galat');
                    redirect(base_url().'user/registrasi');
                }
                else
                {
                    $data=$this->upload->data();
                    $fi[$image]=$data['file_name'];
                }

            }

            $fi['nomor_anggota']='';
            $fi['jenis_anggota']=$this->input->post('jenisanggota');
            $fi['id_jenis_pengurus']=$this->input->post('id_jenis_pengurus');
            $fi['user_id']=$id_user;
            $fi['created_at']=date('Y-m-d H:i:s');

            if ($this->IsNikTerdaftar($this->input->post('nik_ktp'))) {
                $this->session->set_flashdata('error', 'NIK KTP anda telah terdaftar sebagai anggota APKASINDO');
                redirect(base_url().'formulir-pendaftaran','refresh');
            }else{
                $this->db->insert('tb_anggota',$fi);
                $id_anggota = $this->db->insert_id();

                $Insert_verif_data = [
                    'anggota_id'=>$id_anggota,
                    'status'=>0,
                    'created_at'=>NULL,
                    'is_pendaftar'=>'Y'
                ];
                $this->db->insert('tb_anggota_verif',$Insert_verif_data);
                $this->session->set_flashdata('success', 'Anda Berhasil Registrasi, Silahkan Login untuk melakukan transaksi pembelian bibit');
                redirect(base_url().'formulir-pendaftaran','refresh');
            }
            

        }else{
            $GetFieldForm = $this->Formulir('');

            $fi =[];
            $file='';
            foreach ($GetFieldForm as $i => $v) {
                if ($v->type == 'file') {
                    $file=$v->name;
                }
                $fi[$v->name]=$this->input->post($v->name);
            }

            if ( ! $this->upload->do_upload($file))
            {
                $this->session->set_flashdata('warning', 'galat');
                redirect(base_url().'formulir-beli-bibit');
            }
            else
            {
                $data=$this->upload->data();
                $fi[$file]=$data['file_name'];
            }

            $fi['jenis_pembeli']=$this->input->post('jenispembeli');
            $fi['id_jenis_pengurus']=$this->input->post('id_jenis_pengurus');
            $fi['user_id']=$id_user;
            $fi['created_at']=date('Y-m-d');

            if ($this->IsNikTerdaftar($this->input->post('nik_ktp'))) {
                $this->session->set_flashdata('error', 'NIK KTP anda telah terdaftar SISTEM');
                redirect(base_url().'formulir-beli-bibit','refresh');
            }else{
                $this->db->insert('tb_pembeli',$fi);
                $this->session->set_flashdata('success', 'Anda Berhasil Registrasi, Silahkan Login untuk melakukan transaksi pembelian bibit');
                redirect(base_url().'formulir-beli-bibit','refresh');
            }
            
        }

    }

    public function IsNikTerdaftar($nik_ktp)
    {
        $GetDataAnggota = $this->db->get_where('v_anggota_aktif', ['nik_ktp'=>$nik_ktp]);
        $GetDataPembeli = $this->db->get_where('tb_pembeli', ['nik_ktp'=>$nik_ktp]);
        if ($GetDataAnggota->num_rows() > 0) {
            return true;
        }elseif($GetDataPembeli->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    

    public function MakeDirectory($path)
    {
        if (!is_dir($path)) {
            return mkdir($path, 0777, true);
        }
    }

    public function UploadBuktiBayar()
    {
        $config['upload_path']          = './uploads/'.$this->session->userdata('username').'/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';
        $config['file_name'] = 'bukti_bayar_'.time();
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('bukti_bayar'))
        {
            redirect(base_url().'dashboard');
        }
        else
        {
            $data=$this->upload->data();
            $this->db->where('id_verif', $this->input->post('id_verif'));
            $res= $this->db->update('tb_anggota_verif', ['bukti_bayar'=>$data['file_name']]);
            redirect(base_url().'dashboard');
        }

        
    }
}
