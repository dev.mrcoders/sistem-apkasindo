<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    private $template = "template/index";
    private $vpath = "user/";
    public function __construct()
    {
        parent::__construct();
        $this->telegram = new Telegram\Bot\Api('5800933300:AAFp_1eGZXUGSZvaCmB6cHYKjfbVdNgOOzo');
    }
    public function index()
    {
        $data['page']=$this->vpath.'dashboard';
        $this->load->view($this->template,$data);
    }

    public function UpdateData()
    {

        $data['title']='';
        $data['form']=$this->Formulir('anggota');
        $data['data']=$this->db->get_where('v_new_anggota',['user_id'=>$this->session->userdata('user_id')])->row_array();
        $data['page']=$this->vpath.'form_pembaruan';
        $this->load->view($this->template,$data);

    }

    public function UpdateFile($value='')
    {
        $config['upload_path']          = './uploads/'.$this->session->userdata('username').'/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';

        $this->load->library('upload', $config);
        
        if ( ! $this->upload->do_upload('file')){
            $this->session->set_flashdata('warning', 'galat');
            redirect(base_url().'user/dashboard/updatedata');
        }
        else{
            $data = $this->upload->data();
            if ($this->input->post('jenis_') == 'foto_profil') {
                $FileUpdate = [
                    'foto'=>$data['file_name']
                ];
                $this->db->where('id_anggota', $this->input->post('id_anggota'));
                $this->db->update('tb_anggota', $FileUpdate);
            }
            if ($this->input->post('jenis_') == 'bukti_kepemilikan') {
                $FileUpdate = [
                    'bukti_kepemilikan'=>$data['file_name']
                ];
                $this->db->where('id_anggota', $this->input->post('id_anggota'));
                $this->db->update('tb_anggota', $FileUpdate);
            }

            if ($this->input->post('jenis_') == 'bukti_bayar') {
                $FileUpdate = [
                    'bukti_bayar'=>$data['file_name']
                ];
                $this->db->where('anggota_id', $this->input->post('id_anggota'));
                $this->db->update('tb_anggota_verif', $FileUpdate);
            }
            $this->session->set_flashdata('success', 'berhasil');
            redirect(base_url().'user/dashboard/updatedata');
        }
    }

    public function UpdateForm($value='')
    {
        $GetFieldForm = $this->Formulir('anggota');
        $fi =[];
        $fi['jenis_anggota']=$this->input->post('jenisanggota');
        foreach ($GetFieldForm as $i => $v) {
            if ($v->name != 'foto' && $v->name != 'bukti_kepemilikan') {
                $fi[$v->name]=$this->input->post($v->name);
            }
            
        }
        $this->db->where('id_anggota', $this->input->post('id_anggota'));
        $this->db->update('tb_anggota', $fi);
        $this->session->set_flashdata('success', 'berhasil');
        redirect(base_url().'user/dashboard/updatedata');

    }

    public function FinishUpdated($id)
    {
        $statusUpdate = [
            'status'=>0,
        ];
        $this->db->where('anggota_id', $id);
        $this->db->update('tb_anggota_verif', $statusUpdate);

        $response = $this->telegram->sendMessage([
            'chat_id' => '956713352', 
            'text' => 'Hai Admin, User '.$this->session->userdata('username').' Telah melakukan update data mohon di proses data pengajuan anggota.'
        ]);

        $messageId = $response->getMessageId();

        $this->session->set_flashdata('finish', 'berhasil');
        redirect(base_url().'user/dashboard/updatedata');
    }

    public function Formulir($type)
    {
        $form=[];

        if ($type =='anggota') {
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','RT','RW','Provinsi','Kabupaten','Kecamatan','Kelurahan','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 14? 'rata_rata_produksi':($i == 16? 'telepon':$lb))));
                $type = ($i == 11 || $i == 17 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }else{
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Provinsi','Kabupaten','Kecamatan','Nomor HP','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 6? 'telepon':$lb)));
                $type = ($i == 7 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }
        
        $obj = json_decode(json_encode($form));

        return $obj;

    }
}
