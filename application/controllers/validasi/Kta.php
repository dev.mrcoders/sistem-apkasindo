<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kta extends CI_Controller {
	private $path = 'validasi/';
    private $template = "template/index";

    public function __construct()
    {
    	parent::__construct();
    	
    }

	public function index()
	{
		$data['title']='';
		$data['form']=$this->Formulir('anggota');
		$this->db->order_by('id_anggota', 'desc');
        $data['data']=$this->db->get('v_new_anggota')->result_array();
        $data['page']=$this->path.'v_anggota';
		$this->load->view($this->template,$data);
	}

	public function DetailKta($id)
	{
		$data['title']='';
		$data['form']=$this->Formulir('anggota');
        $data['data']=$this->db->get_where('v_new_anggota',['id_anggota'=>$id])->row_array();
        $data['page']=$this->path.'v_verifkta';
		$this->load->view($this->template,$data);
		// $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}


	public function VerifikasiKta()
	{
		$UpdateAnggota = [
			'nomor_anggota'=>$this->input->post('no_anggota')
		];
		$this->db->where('id_anggota', $this->input->post('id_anggota'));
		$this->db->update('tb_anggota', $UpdateAnggota);
		$UpdataVerifikasi=[
			
			'status'=>$this->input->post('verifikasi'),
			'keterangan'=>$this->input->post('alasan')
		];

		$this->db->where('anggota_id', $this->input->post('id_anggota'));
		$this->db->update('tb_anggota_verif', $UpdataVerifikasi);

		redirect(base_url().'master/anggota','refresh');

	}

	public function Formulir($type)
    {
        $form=[];

        if ($type =='anggota') {
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','RT','RW','Provinsi','Kabupaten','Kecamatan','Kelurahan','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 14? 'rata_rata_produksi':($i == 16? 'telepon':$lb))));
                $type = ($i == 11 || $i == 17 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }else{
            $label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Provinsi','Kabupaten','Kecamatan','Nomor HP','Foto'];
            foreach($label as $i=> $lb){
                $name = strtolower(preg_replace('/\s+/', '_', ($i == 6? 'telepon':$lb)));
                $type = ($i == 7 ? 'file':'text');
                $form[] = [
                    'label_name'=>$lb,
                    'name'=>$name,
                    'type'=>$type
                ];


            }
        }
        
        $obj = json_decode(json_encode($form));

        return $obj;

    }

}

/* End of file Kta.php */
/* Location: ./application/controllers/validasi/Kta.php */