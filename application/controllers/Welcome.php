<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$data['page']='halaman/daftaranggota';
		$this->load->view('thema/utama/index',$data);
	}


	public function PendaftaranAnggota($value='')
	{
		$data['page']='halaman/daftaranggota';
		$this->load->view('thema/utama/index',$data);
	}

	public function RegistrasiAnggota($value='')
	{
		$data['form_anggota']=$this->Formulir('anggota');
		$data['page']='halaman/form/registrasi';
		$this->load->view('thema/utama/index',$data);
	}

	public function PembelianBibit($value='')
	{
		$data['page']='halaman/pembelianbibit';
		$this->load->view('thema/utama/index',$data);
	}

	public function RegistrasiTransaksi($value='')
	{
		$data['form_anggota']=$this->Formulir('penangkar');
		$data['page']='halaman/form/registrasitransaksi';
		$this->load->view('thema/utama/index',$data);
	}

	public function temp_kartu($value='')
	{
		$this->load->view('thema/tem_kartu');
	}

	public function Formulir($type)
	{
		$form=[];

		if ($type =='anggota') {
			$label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','RT','RW','Provinsi','Kabupaten','Kecamatan','Kelurahan','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
			foreach($label as $i=> $lb){
				$name = strtolower(preg_replace('/\s+/', '_', ($i == 14? 'rata_rata_produksi':($i == 16? 'telepon':$lb))));
				$type = ($i == 11 || $i == 17 ? 'file':'text');
				$form[] = [
					'label_name'=>$lb,
					'name'=>$name,
					'type'=>$type
				];


			}
		}else{
			$label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Provinsi','Kabupaten','Kecamatan','Nomor HP','Foto'];
			foreach($label as $i=> $lb){
				$name = strtolower(preg_replace('/\s+/', '_', ($i == 6? 'telepon':$lb)));
				$type = ($i == 7 ? 'file':'text');
				$form[] = [
					'label_name'=>$lb,
					'name'=>$name,
					'type'=>$type
				];


			}
		}

		$obj = json_decode(json_encode($form));

		return $obj;

	}

	public function sampleform(Type $var = null)
	{
		$label = ['Nama Lengkap','NIK KTP','Alamat Tinggal','Kecamatan','Kabupten','Provinsi','Lokasi Kebun','Luas Kebun','Bukti Kepemilikan','Tahun Tanam','Asal Bibit','Rata-rata Produksi/Bln/Ha','Nama Koperasi','Telepon/Hp','Foto'];
		foreach($label as $lb){
			$form[]=[
				'label_name'=>$lb,
				'type'=>'text'
			];
		}

		$this->db->insert_batch('tb_formulir',$form);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($form));
	}


	public function CreateBarcode()
	{
		$this->load->library('zend');
		$noanggota =$this->input->get('text');
		echo $this->zend->createbarcode($noanggota);

	}

	public function SaveKta()
	{

		define('UPLOAD_DIR', 'uploads/KTA/');
		$nokta= $this->input->post('noanggota');
		$img = $this->input->post('imgBase64');
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = UPLOAD_DIR . $nokta . '.png';
		$success = file_put_contents($file, $data);
		print $success ? $file : 'Unable to save the file.';

	}	

	public function testing($value='')
	{
		$telegram = new Telegram\Bot\Api('5800933300:AAFp_1eGZXUGSZvaCmB6cHYKjfbVdNgOOzo');
		$caption ='';
		$caption =$caption.'Nama Pembeli : Mahdiawan Nurkholifah'.PHP_EOL;
		$caption =$caption.'Jumlah Pembelian : 2000'.PHP_EOL;

		$response = $telegram->sendPhoto([
			'chat_id' => '956713352', 
			'photo' => 'assets/front-kta.jpeg', 
			'caption' => $caption
		]);

		$messageId = $response->getMessageId();

		$this->output->set_content_type('application/json')->set_output(json_encode($messageId));
	}

	public function JenisKepengurusan()
	{
		$list = $this->db->get('tb_anggota_jenis')->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($list));
		
		
	}
	
}
