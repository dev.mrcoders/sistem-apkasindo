<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    private $template = "template/index";
    public function __construct()
    {
        parent::__construct();
                
    }
	public function index()
	{
        $data['title']='';
        $verifikasi=NULL;
        if ($this->session->userdata('level')==4 || $this->session->userdata('level')==5) {
            $data['isverif'] = $this->db->get_where('v_verif_anggota',['user_id'=>$this->session->userdata('user_id')])->row();
            $data['data']=$this->db->get_where('tb_anggota',['user_id'=>$this->session->userdata('user_id')])->row();
        }
        if ($this->session->userdata('level')==3 || $this->session->userdata('level')==5) {
            $data['isverif'] = $this->db->get_where('tb_pembeli',['user_id'=>$this->session->userdata('user_id')])->row();
            $data['data']=$this->db->get_where('tb_pembeli',['user_id'=>$this->session->userdata('user_id')])->row();
        }

        $data['page']='dashboard';
		$this->load->view($this->template,$data);
        
	}

    public function GetDataAnggota($value='')
    {
        $this->db->where('jenis_anggota',1);
        $this->db->from('v_anggota_aktif');
        $pengurus = $this->db->count_all_results();

        $this->db->where('jenis_anggota',2);
        $this->db->from('v_anggota_aktif');
        $anggota = $this->db->count_all_results();

        $this->db->where('jenis_pembeli',2);
        $this->db->from('tb_pembeli');
        $petani = $this->db->count_all_results();

        $this->db->where('jenis_pembeli',1);
        $this->db->from('tb_pembeli');
        $penangkar = $this->db->count_all_results();

        $this->db->where('status_pembayaran','unpaid');
        $this->db->from('tb_transaksi_invoice');
        $unpaid = $this->db->count_all_results();

        $this->db->where('status_pembayaran','proses');
        $this->db->from('tb_transaksi_invoice');
        $proses = $this->db->count_all_results();

        $this->db->where('status_pembayaran','success');
        $this->db->from('tb_transaksi_invoice');
        $success = $this->db->count_all_results();

        $this->db->where('is_pendaftar','Y');
        $this->db->from('tb_anggota_verif');
        $total_daftar = $this->db->count_all_results();

        $this->db->where('is_pendaftar','Y');
        $this->db->where('status',1);
        $this->db->from('tb_anggota_verif');
        $accepted = $this->db->count_all_results();

        $this->db->where('is_pendaftar','Y');
        $this->db->where('status',2);
        $this->db->from('tb_anggota_verif');
        $rejected = $this->db->count_all_results();

        $result = [
            'anggota'=>$anggota,
            'pengurus'=>$pengurus,
            'penangkar'=>$penangkar,
            'petani'=>$petani,
            'unpaid'=>$unpaid,
            'prosess'=>$proses,
            'success'=>$success,
            'total'=>$total_daftar,
            'accepted'=>$accepted,
            'rejected'=>$rejected,
        ];
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }



}
