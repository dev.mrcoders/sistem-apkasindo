<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zend {

	public function __construct($class = NULL)
	{
		ini_set('include_path',
		ini_get('include_path'). PATH_SEPARATOR. APPPATH. 'libraries');
	
		if($class)
		{
			require_once(string) $class.'.php'; //fixed CI 3 issue by lilsammy
			log_message('debug', "Zend Class $class Loaded");
		}else
		{
			log_message('debug', "Zend Class Initialized");
		}
	}

	public function load($class)
	{
		require_once(string) $class.'.php'; //fixed CI 3 issue by lilsammy
		log_message('debug', "Zend Class $class Loaded");


	}

	public function createbarcode($value)
	{
		$this->load('Zend/Barcode');
		//generate barcode
		$imageResource = Zend_Barcode::factory('code128', 'image', array('text'=>$value), array())->draw();
		imagepng($imageResource, 'uploads/barcode/'.$value.'.png');

		return base_url().'uploads/barcode/'.$value.'.png';
	}


}
